using System.Collections.Generic;
using NUnit.Framework;
using UMF.DelegateBinders;

namespace UMF.Tests
{
	public class MultiDelegates
	{
		#region Initialization data

		private delegate void AdderDelegate(ref int i);

		private readonly List<AdderDelegate> adders;
		private readonly AdderDelegate adder;

		public MultiDelegates()
		{
			adders = new List<AdderDelegate>();
			adder = ReflectionHelpers.CreateMethod<AdderDelegate>(this, nameof(Adder));
		}

		#endregion

		#region Callers

		[TestCase(0, TestName = "Warmup")]
		[TestCase(0, TestName = "None")]
		[TestCase(1, TestName = "Single")]
		[TestCase(2, TestName = "Double")]
		public void AdderCaller(int count)
		{
			adders.Clear();
			for (int i = 0; i < count; i++) adders.Add(adder);

			int result = 0;
			ReflectionHelpers.CreateMutliMethod(adders.ToArray())(ref result);
			Assert.IsTrue(result == count);
		}

		#endregion

		#region Targets

		public void Adder(ref int i) => i++;

		#endregion
	}
}
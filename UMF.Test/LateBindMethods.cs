﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using NUnit.Framework;
using UMF.DelegateBinders;
using UMF.DelegateBinders.Exceptions;

namespace UMF.Tests
{
	[SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Global")]
	public class LateBindMethods
	{
		#region Initialization data

		private const string kArg1 = "1";
		private const string kArg2 = "2";
		private const uint kArg3 = 1730;
		private const int kResult = 1471;

		private delegate int TestDelegate(string s1, string s2, uint u3);
		private delegate void RefTestDelegate(ref bool b);
		private delegate void OutTestDelegate(out bool b);

		#endregion

		#region Argument checks

		private void CheckMethod(string methodName, BindingFlags flags = BindingFlags.Public | BindingFlags.Instance) =>
			Assert.IsTrue(
				ReflectionHelpers.CreateMethod<TestDelegate>(
					this,
					methodName,
					flags
				)(kArg1, kArg2, kArg3) == kResult
			);

		private void CheckMethodLossy(string methodName, BindingFlags flags = BindingFlags.Public | BindingFlags.Instance) =>
			Assert.IsTrue(
				ReflectionHelpers.CreateMethodLossy<TestDelegate>(
					this,
					methodName,
					flags
				)(kArg1, kArg2, kArg3) == kResult
			);

		private static void CheckArg1(string arg) =>
			Assert.IsTrue(arg == kArg1);

		private static void CheckArg2(string arg) =>
			Assert.IsTrue(arg == kArg2);

		private static void CheckArg3(uint arg) =>
			Assert.IsTrue(arg == kArg3);

		#endregion

		#region Callers

		[Test]
		public void FullFullCaller() =>
			CheckMethod(nameof(Full));

		[Test]
		public void FullLossyCaller() =>
			CheckMethodLossy(nameof(Full));

		[Test]
		public void TruncatedCaller() =>
			Assert.Throws<TargetParameterCountException>(() => CheckMethod(nameof(Truncated)));

		[Test]
		public void TruncatedUnorderedCaller() =>
			CheckMethodLossy(nameof(TruncatedUnordered));

		[Test]
		public void TruncatedLossyCaller() =>
			CheckMethodLossy(nameof(Truncated));

		[Test]
		public void StaticInstanceSearchCaller() =>
			Assert.Throws<MissingMethodException>(() => CheckMethodLossy(nameof(Static)));

		[Test]
		public void StaticStaticSearchCaller() =>
			CheckMethodLossy(nameof(Static), BindingFlags.Public | BindingFlags.Static);

		[Test]
		public void InvalidTypeCaller() =>
			Assert.Throws<TargetParameterTypeException>(() => CheckMethodLossy(nameof(InvalidType)));

		[Test]
		public void TooManyArgsCaller() =>
			Assert.Throws<TargetParameterCountException>(() => CheckMethodLossy(nameof(TooManyArgs)));

		[Test]
		public void InvalidArgsCaller() =>
			Assert.Throws<TargetParameterNameException>(() => CheckMethodLossy(nameof(InvalidArgs)));

		[Test]
		public void InvalidReturnCaller() =>
			Assert.Throws<TargetReturnException>(() => CheckMethodLossy(nameof(InvalidReturn)));

		[Test]
		public void RefCaller()
		{
			bool b = false;
			ReflectionHelpers.CreateMethod<RefTestDelegate>(this, nameof(Ref))(ref b);
			Assert.IsTrue(b);
		}

		[Test]
		public void RefTruncatedCaller()
		{
			bool b = false;
			ReflectionHelpers.CreateMethodLossy<RefTestDelegate>(this, nameof(RefTruncated))(ref b);
			Assert.IsFalse(b);
		}

		[Test]
		public void OutCaller()
		{
			ReflectionHelpers.CreateMethod<OutTestDelegate>(this, nameof(Out))(out bool b);
			Assert.IsTrue(b);
		}

		[Test]
		public void OutInvalidAccessCaller() =>
			Assert.Throws<TargetParameterAccessException>(() => ReflectionHelpers.CreateMethod<OutTestDelegate>(this, nameof(OutInvalidAccess)));

		#endregion

		#region Targets

		public int Full(string s1, string s2, uint u3)
		{
			CheckArg1(s1);
			CheckArg2(s2);
			CheckArg3(u3);

			return kResult;
		}

		public int Truncated(string s1)
		{
			CheckArg1(s1);

			return kResult;
		}

		public int TruncatedUnordered(string s2, string s1)
		{
			CheckArg2(s2);
			CheckArg1(s1);

			return kResult;
		}

		public static int Static(string s1, string s2, uint u3)
		{
			CheckArg1(s1);
			CheckArg2(s2);
			CheckArg3(u3);

			return kResult;
		}

		public int InvalidType(Stream s1)
		{
			return kResult;
		}

		public int TooManyArgs(string s1, string s2, uint u3, uint u4)
		{
			throw new InvalidOperationException("An exception should be thrown for too many arguments.");
		}

		public int InvalidArgs(string misnamed1)
		{
			throw new InvalidOperationException("An exception should be thrown for invalid argument names.");
		}

		public void InvalidReturn(string s1, string s2, uint u3)
		{
			throw new InvalidOperationException("An exception should be thrown for invalid return type.");
		}

		public void Ref(ref bool b)
		{
			b = true;
		}

		public void RefTruncated(bool b) { }

		public void Out(out bool b)
		{
			b = true;
		}

		public void OutInvalidAccess(bool b)
		{
			throw new InvalidOperationException("This code should never be called, only the binding should be tested.");
		}

		#endregion
	}
}
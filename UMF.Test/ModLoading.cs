using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using NUnit.Framework;
using UMF.Core;
using UMF.Core.Log;
using UMF.DelegateBinders;
using UMF.Events;
using UMF.Mods;

namespace UMF.Tests
{
	public class ModLoading
	{
		private delegate void OnHurt(ref bool proceed);

		public ModLoading()
		{
			if (!Directory.Exists("UMF"))
			{
				Directory.CreateDirectory("UMF");
			}

			if (!Directory.Exists("UMF\\mods"))
			{
				Directory.CreateDirectory("UMF\\mods");
			}

			new ManagedUmfModule(typeof(UmfCore)).Init();
		}

		[Test]
		public void LoadingEventsCaller()
		{
			TestMod mod = new TestMod();

			UmfCore.Instance.Load(mod);
			Assert.IsTrue(mod.loaded);
			UmfCore.Instance.Unload(mod);
			Assert.IsTrue(mod.unloaded);
		}

		[Test]
		public void RefreshEventsCaller()
		{
			Stopwatch sw = new Stopwatch();

			sw.Start();
			EventModule.Instance.RebuildEvents();
			sw.Stop();

			Console.WriteLine("Warmup (rough): " + sw.Elapsed.TotalMilliseconds);
			sw.Reset();

			GC.Collect();
			GC.WaitForPendingFinalizers();

			sw.Start();
			EventModule.Instance.RebuildEvents();
			sw.Stop();

			Console.WriteLine("JIT Compiled: " + sw.Elapsed.TotalMilliseconds);
			sw.Reset();
		}

		[SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
		[Test]
		public void EventCaller()
		{
			TestMod mod = new TestMod();

			UmfCore.Instance.Load(mod);

			bool proceed = false;
			#pragma warning disable 219
			float damage = 0;
			#pragma warning restore 219

			Stopwatch sw = new Stopwatch();

			sw.Start();
			mod.OnHurt(proceed);
			sw.Stop();

			Console.WriteLine("Direct warmup (rough): " + sw.Elapsed.TotalMilliseconds);
			sw.Reset();

			GC.Collect();
			GC.WaitForPendingFinalizers();

			sw.Start();
			mod.OnHurt(proceed);
			sw.Stop();

			Console.WriteLine("Direct JIT compiled: " + sw.Elapsed.TotalMilliseconds);
			sw.Reset();

			OnHurt onHurtDelegate = ReflectionHelpers.CreateMethodLossy<OnHurt>(mod, nameof(TestMod.OnHurt));

			sw.Start();
			onHurtDelegate(ref proceed);
			sw.Stop();

			Console.WriteLine("Delegate warmup (rough): " + sw.Elapsed.TotalMilliseconds);
			sw.Reset();

			GC.Collect();
			GC.WaitForPendingFinalizers();

			sw.Start();
			onHurtDelegate(ref proceed);
			sw.Stop();

			Console.WriteLine("Delegate JIT compiled: " + sw.Elapsed.TotalMilliseconds);
			sw.Reset();

			Assert.IsFalse(proceed);

			UmfCore.Instance.Unload(mod);
		}

		[Test]
		public void BindFailCaller()
		{
			UmfMod mod = new BindFailTestMod();

			bool errored = false;
			Logger.Invoked += message => errored = errored || message.message.StartsWith("Failed to bind");

			UmfCore.Instance.Load(mod);
			UmfCore.Instance.Unload(mod);

			Assert.IsTrue(errored);
		}

		[Test]
		public void NonCoreCaller()
		{
			UmfMod mod = new NonCoreTestMod();

			bool noncored = false;
			Logger.Invoked += message => noncored = noncored || message.message.Contains($"{nameof(NonCore)} attribute.");

			UmfCore.Instance.Load(mod);
			UmfCore.Instance.Unload(mod);

			Assert.IsTrue(noncored);
		}
	}

	internal class TestMod : UmfMod
	{
		public bool loaded;
		public bool unloaded;

		private void OnLoadStart()
		{
			throw new InvalidOperationException("Mod should not be loaded when the load start event is called.");
		}

		private void OnLoadFinish(UmfMod mod)
		{
			if (this == mod) loaded = true;
		}

		private void OnUnloadStart(UmfMod mod)
		{
			if (this == mod) unloaded = true;
		}

		private void OnUnloadFinish()
		{
			throw new InvalidOperationException("Mod should not be unloaded when the unload finish event is called.");
		}

		public void OnHurt(bool proceed)
		{
			Info("Hurt is proceeding: " + proceed);
		}
	}

	internal class BindFailTestMod : UmfMod
	{
		public void OnLoadFinish(UmfMod mod, int garbo)
		{
			throw new InvalidOperationException("This should fail to bind and not run because of that.");
		}
	}

	internal class NonCoreTestMod : UmfMod
	{
		[NonCore]
		public void OnLoadFinish(UmfMod mod, int garbo)
		{
			throw new InvalidOperationException("This should fail to bind and not run because of that.");
		}

		public void OnLoadFinish(UmfMod mod)
		{
			Info("Load finished ran: " + mod);
		}
	}
}
﻿using System;
using System.IO;
using System.Reflection;
using dnlib.DotNet;
using UnityEngine;
using Console = GameConsole.Console;

namespace UMF.Patcher.SCPSL
{
	public class SCPSLPatcher : BasePatcher
	{
		public override string CurrentVersion { get; }
		public override string DefaultPath { get; }
		public override MethodDef InitMethod { get; protected set; }
		public override MethodDef ModLoader { get; }
		public override int ILIndex { get; }

		public SCPSLPatcher()
		{
			CurrentVersion = "0.0.0";
			DefaultPath = "../../SCPSL_Data/Managed/Assembly-CSharp.dll";

			ModLoader = ModuleDefMD.Load(typeof(SCPSLPayload).Module).Find(typeof(SCPSLPayload).FullName, true).FindMethod(nameof(SCPSLPayload.UMFModLoader));
			ILIndex = 0;
		}

		public override void LoadModule(ModuleDefMD module)
		{
			InitMethod = module.Find("GameConsole.Console", true).FindMethod("Start");
		}

		private static class SCPSLPayload
		{
			internal static void UMFModLoader()
			{
				Console.singleton.AddLog("Running UMF Mod Loader...", Color.magenta);

				try
				{
					foreach (string file in Directory.GetFiles("UMF/bin/", "*.dll"))
					{
						Console.singleton.AddLog($"Loading {file}...", Color.magenta);

						Assembly.LoadFrom(file);
					}

					foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
					{
						Type core = assembly.GetType("UMF.Core.UmfCore");
						if (core != null)
						{
							MethodInfo method = core.GetMethod("Init", BindingFlags.NonPublic | BindingFlags.Static);
							if (method == null) throw new MissingMethodException("The Init method of UmfCore does not exist.");

							method.Invoke(null, null);
						}
					}
				}
				catch (Exception e)
				{
					Console.singleton.AddLog($"Failed to load UMF:\n{e}", Color.magenta);
				}
			}
		}
	}
}
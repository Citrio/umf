﻿using RoR2;
using UMF.Mods;

namespace UMF.Example.RoR2
{
	[UmfModInfo(
		Name = "Risk of Rain 2 Example",
		Author = "Androx",
		Description = "An example of UMF using RoR2"
	)]
	public class Mod : UmfMod
	{
		public Mod() : base(new EchoCommand("echo")) { }

		public void OnConsoleCmd(NetworkUser sender, string cmd)
		{
			Info($"\"{sender.userName}\" used command \"{cmd}\"");
		}
	}
}
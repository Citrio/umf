using System.Text;
using UMF.Commands;
using UMF.Commands.Arguments;
using UMF.Commands.Options;

namespace UMF.Example.RoR2
{
	public class EchoCommand : UmfCommand<Mod>
	{
		public override string Description { get; } = "Reads back info.";

		public ValueArgument ToEcho { get; }
		public ToggleOption ObnoxiousMode { get; }
		public ToggleOption QuietMode { get; }
		public ValueOption OtherMode { get; }

		public EchoCommand(string name) : base(name)
		{
			MandatoryArguments.Add(ToEcho = new ValueArgument("info", "Info to spew out."));

			Options.Add(ObnoxiousMode = new ToggleOption('o', "obnoxious", "MAKES THE COMMAND REALLY LOUD."));
			Options.Add(QuietMode = new ToggleOption('q', "quiet", "makes the command really quiet."));
			Options.Add(OtherMode = new ValueOption(null, "other", "The other thing to say."));
		}

		protected override string Execute()
		{
			StringBuilder builder = new StringBuilder();

			builder.Append("Info: ");
			builder.Append(ToEcho.Value);

			builder.AppendLine();

			if (ObnoxiousMode.HasValue)
			{
				builder.AppendLine("Obnoxious mode.");
			}

			if (QuietMode.HasValue)
			{
				builder.AppendLine("Quiet mode.");
			}

			if (OtherMode.HasValue)
			{
				builder.Append("Other info: ");
				builder.Append(OtherMode.Value);

				builder.AppendLine();
			}

			return builder.ToString();
		}
	}
}
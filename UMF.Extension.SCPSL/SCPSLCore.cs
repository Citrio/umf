﻿using System;
using UMF.Core;
using UMF.Core.Log;
using UnityEngine;
using Console = GameConsole.Console;
using Logger = UMF.Core.Log.Logger;

namespace UMF.Extension.SCPSL
{
	[UmfExtension]
	public class SCPSLCore : UmfModule
	{
		public static SCPSLCore Instance { get; private set; }

		public override string SettingsId { get; } = "Extension.SCPSL";

		private static void Init()
		{
			if (Instance != null) throw new InvalidOperationException("Already instantiated.");

			Instance = new SCPSLCore();

			Logger.Invoked += message =>
			{
				string noSeverity = "\n" + message.ToString(LogMessage.Format.Source | LogMessage.Format.Message);
				switch (message.severity)
				{
					case Logger.Severity.Error:
						Console.singleton.AddLog(noSeverity, Color.red);
						break;

					case Logger.Severity.Warning:
						Console.singleton.AddLog(noSeverity, Color.yellow);
						break;

					case Logger.Severity.Debug:
						Console.singleton.AddLog(noSeverity, Color.magenta);
						break;

					default:
						Console.singleton.AddLog(noSeverity, Color.grey);
						break;
				}
			};

			Application.logMessageReceived += (condition, trace, type) =>
			{
				const string unitySource = "Unity";

				switch (type)
				{
					case LogType.Exception: // Harmony hook code could through exceptions that aren't logged to console.
						string full = condition + Environment.NewLine + trace;

						if (UmfCore.Instance.Settings.debug)
						{
							Logger.Error(unitySource, full);
						}
						else
						{
							Logger.ErrorSilent(unitySource, full);
						}
						break;

					case LogType.Error:
						if (UmfCore.Instance.Settings.debug)
						{
							Logger.Error(unitySource, condition);
						}
						else
						{
							Logger.ErrorSilent(unitySource, condition);
						}
						break;

					case LogType.Warning:
						if (UmfCore.Instance.Settings.debug)
						{
							Logger.Warning(unitySource, condition);
						}
						else
						{
							Logger.WarningSilent(unitySource, condition);
						}
						break;

					case LogType.Assert:
					case LogType.Log:
						if (UmfCore.Instance.Settings.debug)
						{
							Logger.Info(unitySource, condition);
						}
						else
						{
							Logger.InfoSilent(unitySource, condition);
						}
						break;
				}
			};
		}
	}
}
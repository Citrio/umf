using UMF.Events;
using UnityEngine;

namespace UMF.Extension.SCPSL.Hooks
{
	public class PlayerRemove : UmfHarmonyEvent
	{
		public delegate void OnPlayerRemove(GameObject player);

		public PlayerRemove() : base(typeof(OnPlayerRemove), typeof(PlayerManager), nameof(PlayerManager.RemovePlayer)) { }

		private static void Prefix(GameObject player)
		{
			if (Locked) return;

			Handle<OnPlayerRemove>()?.Invoke(player);
		}
	}
}
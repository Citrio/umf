using UMF.Events;
using UnityEngine;

namespace UMF.Extension.SCPSL.Hooks
{
	public class PlayerAdd : UmfHarmonyEvent
	{
		public delegate void OnPlayerAdd(GameObject player);

		public PlayerAdd() : base(typeof(OnPlayerAdd), typeof(PlayerManager), nameof(PlayerManager.AddPlayer)) { }

		private static void Postfix(GameObject player)
		{
			if (Locked) return;

			Handle<OnPlayerAdd>()?.Invoke(player);
		}
	}
}
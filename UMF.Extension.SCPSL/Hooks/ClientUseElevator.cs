using UMF.Events;
using UnityEngine;

namespace UMF.Extension.SCPSL.Hooks
{
	public class ClientUseElevator : UmfHarmonyEvent
	{
		public delegate void OnClientUseElevator(Lift lift, ref bool allow);

		public ClientUseElevator() : base(typeof(OnClientUseElevator), typeof(PlayerInteract), nameof(PlayerInteract.CallCmdUseElevator)) { }

		private static bool Prefix(GameObject elevator)
		{
			if (Locked) return true;

			Lift lift = elevator.GetComponent<Lift>();

			bool allow = true;
			Handle<OnClientUseElevator>()?.Invoke(lift, ref allow);

			return allow;
		}
	}
}
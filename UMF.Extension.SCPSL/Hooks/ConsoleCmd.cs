using UMF.Commands;
using UMF.Commands.Results;
using UMF.Events;
using UnityEngine;
using Console = GameConsole.Console;

namespace UMF.Extension.SCPSL.Hooks
{
	public class ConsoleCmd : UmfHarmonyEvent
	{
		private static readonly Color32 _colorSuccess = Color.green;
		private static readonly Color32 _colorError = Color.red;

		public delegate void OnConsoleCmd(string cmd, ref bool handled, ref string result, ref bool success);

		public ConsoleCmd() : base(typeof(OnConsoleCmd), typeof(Console), nameof(Console.TypeCommand)) { }

		private static void LogMessage(string message, bool success) =>
			Console.singleton.AddLog(
				message,
				success
					? _colorSuccess
					: _colorError
			);

		private static bool Prefix(string cmd, ref string __result)
		{
			if (Locked) return true;

			string result = null;
			bool handled = false;
			bool success = true;
			Handle<OnConsoleCmd>()?.Invoke(cmd, ref handled, ref result, ref success);

			if (result != null)
			{
				LogMessage(result, success);
			}

			if (handled)
			{
				__result = string.Empty;
				return false;
			}

			CommandResult commandResult = CommandModule.Instance.Handle(cmd);

			if (commandResult.found)
			{
				LogMessage(commandResult.message, commandResult.success);
				__result = string.Empty;
				return false;
			}

			return true;
		}
	}
}
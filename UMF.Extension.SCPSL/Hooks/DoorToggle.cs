using UMF.Events;

namespace UMF.Extension.SCPSL.Hooks
{
	public class DoorToggle : UmfHarmonyEvent
	{
		public delegate void OnDoorToggle(Door door);

		public DoorToggle() : base(typeof(OnDoorToggle), typeof(Door), nameof(Door.SetState)) { }

		private static void Prefix(Door __instance)
		{
			if (Locked) return;

			Handle<OnDoorToggle>()?.Invoke(__instance);
		}
	}
}
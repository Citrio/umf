using UMF.Events;

namespace UMF.Extension.SCPSL.Hooks
{
	public class ClientSwitchWarhead : UmfHarmonyEvent
	{
		public delegate void OnClientSwitchWarheadLever(ref bool allow);

		public ClientSwitchWarhead() : base(typeof(OnClientSwitchWarheadLever), typeof(PlayerInteract), nameof(PlayerInteract.CallCmdUsePanel)) { }

		private static bool Prefix(string n)
		{
			if (Locked) return true;

			if (!n.Contains("lever")) return true;

			bool allow = true;
			Handle<OnClientSwitchWarheadLever>()?.Invoke(ref allow);

			return allow;
		}
	}
}
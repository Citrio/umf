using UMF.Events;
using UnityEngine;

namespace UMF.Extension.SCPSL.Hooks
{
	public class ClientGeneratorAccess : UmfHarmonyEvent
	{
		public delegate void OnClientGeneratorAccess(Generator079 generator, ref bool allow);

		public ClientGeneratorAccess() : base(typeof(OnClientGeneratorAccess), typeof(PlayerInteract), nameof(PlayerInteract.CallCmdUseGenerator)) { }

		private static bool Prefix(string command, GameObject go)
		{
			if (Locked) return true;

			if (!command.Contains("EPS_DOOR")) return true;

			Generator079 generator = go.GetComponent<Generator079>();

			bool allow = true;
			Handle<OnClientGeneratorAccess>()?.Invoke(generator, ref allow);

			return allow;
		}
	}
}
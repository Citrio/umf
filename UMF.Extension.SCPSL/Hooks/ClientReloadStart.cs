using UMF.Events;

namespace UMF.Extension.SCPSL.Hooks
{
	public class ClientReloadStart : UmfHarmonyEvent
	{
		public delegate void OnClientReloadStart(ref bool allow);

		public ClientReloadStart() : base(typeof(OnClientReloadStart), typeof(WeaponManager), nameof(WeaponManager.CallCmdReload)) { }

		private static bool Prefix(bool animationOnly)
		{
			if (Locked) return true;

			if (!animationOnly) return true;

			bool allow = true;
			Handle<OnClientReloadStart>()?.Invoke(ref allow);

			return allow;
		}
	}
}
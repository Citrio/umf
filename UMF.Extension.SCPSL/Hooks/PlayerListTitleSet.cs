using UMF.Events;

namespace UMF.Extension.SCPSL.Hooks
{
	public class PlayerListTitleSet : UmfHarmonyEvent
	{
		public delegate void OnPlayerListTitleSet(ref string title, string previousTitle);

		public PlayerListTitleSet() : base(typeof(OnPlayerListTitleSet), typeof(PlayerList), "set_" + nameof(PlayerList.NetworksyncServerName)) { }

		private static void Prefix(PlayerList __instance, ref string value)
		{
			if (Locked) return;

			Handle<OnPlayerListTitleSet>()?.Invoke(ref value, __instance.syncServerName);
		}
	}
}
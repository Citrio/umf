using UMF.Events;

namespace UMF.Extension.SCPSL.Hooks
{
	public class FacilityLoad : UmfHarmonyEvent
	{
		public delegate void OnFacilityLoad();

		public FacilityLoad() : base(typeof(OnFacilityLoad), typeof(CharacterClassManager), "Start") { }

		private static void Postfix(CharacterClassManager __instance)
		{
			if (Locked) return;

			if (!__instance.isLocalPlayer) return;

			Handle<OnFacilityLoad>()?.Invoke();
		}
	}
}
using UMF.Events;

namespace UMF.Extension.SCPSL.Hooks
{
	public class ClientReloadFinish : UmfHarmonyEvent
	{
		public delegate void OnClientReloadFinish(ref bool allow);

		public ClientReloadFinish() : base(typeof(OnClientReloadFinish), typeof(WeaponManager), nameof(WeaponManager.CallCmdReload)) { }

		private static bool Prefix(bool animationOnly)
		{
			if (Locked) return true;

			if (animationOnly) return true;

			bool allow = true;
			Handle<OnClientReloadFinish>()?.Invoke(ref allow);

			return allow;
		}
	}
}
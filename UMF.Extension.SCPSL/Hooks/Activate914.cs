using UMF.Events;

namespace UMF.Extension.SCPSL.Hooks
{
	public class Activate914 : UmfHarmonyEvent
	{
		public delegate void On914Activate();

		public Activate914() : base(typeof(On914Activate), typeof(PlayerInteract), nameof(PlayerInteract.CallRpcUse914)) { }

		private static void Prefix()
		{
			if (Locked) return;

			Handle<On914Activate>()?.Invoke();
		}
	}
}
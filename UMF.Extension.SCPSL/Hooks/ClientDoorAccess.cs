using UMF.Events;
using UnityEngine;

namespace UMF.Extension.SCPSL.Hooks
{
	public class ClientDoorAccess : UmfHarmonyEvent
	{
		public delegate void OnClientDoorAccess(Door door, ref bool allow);

		public ClientDoorAccess() : base(typeof(OnClientDoorAccess), typeof(PlayerInteract), nameof(PlayerInteract.CallCmdOpenDoor)) { }

		private static bool Prefix(GameObject doorId)
		{
			if (Locked) return true;

			Door door = doorId.GetComponent<Door>();

			bool allow = true;
			Handle<OnClientDoorAccess>()?.Invoke(door, ref allow);

			return allow;
		}
	}
}
using UMF.Events;
using UnityEngine;

namespace UMF.Extension.SCPSL.Hooks
{
	public class ClientShoot : UmfHarmonyEvent
	{
		public delegate void OnClientShoot(ref GameObject target, ref string hitbox, ref Vector3 source, ref Vector3 destination, ref Vector3 direction, ref bool allow);

		public ClientShoot() : base(typeof(OnClientShoot), typeof(WeaponManager), nameof(WeaponManager.CallCmdShoot)) { }

		private static bool Prefix(ref GameObject target, ref string hitboxType, ref Vector3 dir, ref Vector3 sourcePos, ref Vector3 targetPos)
		{
			if (Locked) return true;

			bool allow = true;
			Handle<OnClientShoot>()?.Invoke(ref target, ref hitboxType, ref sourcePos, ref targetPos, ref dir, ref allow);

			return allow;
		}
	}
}
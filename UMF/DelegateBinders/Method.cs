using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using UMF.DelegateBinders.Exceptions;

namespace UMF.DelegateBinders
{
	/// <summary>
	/// Helpful collection of methods to do late-binding without the performance downsides.
	/// </summary>
	public static partial class ReflectionHelpers
	{
		#region Exposed

		#region Instance

		/// <summary>
		/// Generates a delegate bound to a named method in the instance.
		/// </summary>
		/// <param name="delegateType">The type of the delegate to bind to the target.</param>
		/// <param name="instance">Instance the bind target is in.</param>
		/// <param name="name">Name of the bind target.</param>
		/// <param name="flags">The binding flags to search methods by.</param>
		/// <returns>A delegate to be used for fast-latebound invocation.</returns>
		/// <exception cref="ArgumentNullException">One of the arguments provided was null.</exception>
		/// <exception cref="TargetParameterCountException">The binding target has unequal parameters to <paramref name="delegateType"/>.</exception>
		/// <exception cref="TargetParameterNameException">A binding target parameter is not found in the <paramref name="delegateType"/>.</exception>
		/// <exception cref="TargetParameterTypeException">A binding target parameter has a different type than the parameter found in the <paramref name="delegateType"/>.</exception>
		/// <exception cref="TargetParameterAccessException">A binding target parameter has a different access (in/out/ref) than the parameter found in the <paramref name="delegateType"/>.</exception>
		public static Delegate CreateMethod(Type delegateType, object instance, string name, BindingFlags flags = BindingFlags.Public | BindingFlags.Instance)
		{
			if (delegateType == null) throw new ArgumentNullException(nameof(delegateType));

			return CreateMethod(delegateType, GetMethodInfo(instance, name, flags), instance);
		}

		/// <summary>
		/// Generates a delegate bound to a named method in the instance.
		/// </summary>
		/// <param name="delegateType">The type of the delegate to bind to the target.</param>
		/// <param name="instance">Instance the bind target is in.</param>
		/// <param name="name">Name of the bind target.</param>
		/// <param name="flags">The binding flags to search methods by.</param>
		/// <returns>A delegate to be used for fast-latebound invocation.</returns>
		/// <exception cref="ArgumentNullException">One of the arguments provided was null.</exception>
		/// <exception cref="TargetParameterCountException">The binding target has more parameters than <paramref name="delegateType"/>.</exception>
		/// <exception cref="TargetParameterNameException">A binding target parameter is not found in the <paramref name="delegateType"/>.</exception>
		/// <exception cref="TargetParameterTypeException">A binding target parameter has a different type than the parameter found in the <paramref name="delegateType"/>.</exception>
		/// <exception cref="TargetParameterAccessException">A binding target parameter has a different access (in/out/ref) than the parameter found in the <paramref name="delegateType"/>.</exception>
		public static Delegate CreateMethodLossy(Type delegateType, object instance, string name, BindingFlags flags = BindingFlags.Public | BindingFlags.Instance)
		{
			if (delegateType == null) throw new ArgumentNullException(nameof(delegateType));

			return CreateMethodLossy(delegateType, GetMethodInfo(instance, name, flags), instance);
		}

		/// <summary>
		/// Generates a delegate bound to a named method in the instance.
		/// </summary>
		/// <param name="instance">Instance the bind target is in.</param>
		/// <param name="name">Name of the bind target.</param>
		/// <param name="flags">The binding flags to search methods by.</param>
		/// <typeparam name="TDelegate">The type of the delegate to bind to the target.</typeparam>
		/// <returns>A delegate to be used for fast-latebound invocation.</returns>
		/// <exception cref="ArgumentNullException">One of the arguments provided was null.</exception>
		/// <exception cref="TargetParameterCountException">The binding target has unequal parameters to <typeparamref name="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterNameException">A binding target parameter is not found in the delegate type.</exception>
		/// <exception cref="TargetParameterTypeException">A binding target parameter has a different type than the parameter found in the delegate type.</exception>
		/// <exception cref="TargetParameterAccessException">A binding target parameter has a different access (in/out/ref) than the parameter found in the delegate type.</exception>
		public static TDelegate CreateMethod<TDelegate>(object instance, string name, BindingFlags flags = BindingFlags.Public | BindingFlags.Instance) where TDelegate : Delegate =>
			CreateMethod<TDelegate>(GetMethodInfo(instance, name, flags), instance);

		/// <summary>
		/// Generates a delegate bound to a named method in the instance.
		/// </summary>
		/// <param name="instance">Instance the bind target is in.</param>
		/// <param name="name">Name of the bind target.</param>
		/// <param name="flags">The binding flags to search methods by.</param>
		/// <typeparam name="TDelegate">The type of the delegate to bind to the target.</typeparam>
		/// <returns>A delegate to be used for fast-latebound invocation.</returns>
		/// <exception cref="ArgumentNullException">One of the arguments provided was null.</exception>
		/// <exception cref="TargetParameterCountException">The binding target has more parameters than <typeparamref name="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterNameException">A binding target parameter is not found in the delegate type.</exception>
		/// <exception cref="TargetParameterTypeException">A binding target parameter has a different type than the parameter found in the delegate type.</exception>
		/// <exception cref="TargetParameterAccessException">A binding target parameter has a different access (in/out/ref) than the parameter found in the delegate type.</exception>
		public static TDelegate CreateMethodLossy<TDelegate>(object instance, string name, BindingFlags flags = BindingFlags.Public | BindingFlags.Instance) where TDelegate : Delegate =>
			CreateMethodLossy<TDelegate>(GetMethodInfo(instance, name, flags), instance);

		#endregion

		#region Static

		/// <summary>
		/// Generates a delegate bound to a named static method in the type.
		/// </summary>
		/// <param name="delegateType">The delegate to bind to the target.</param>
		/// <param name="type">Type the static bind target is in.</param>
		/// <param name="name">Name of the bind target.</param>
		/// <param name="flags">The binding flags to search methods by.</param>
		/// <returns>A delegate to be used for fast-latebound invocation.</returns>
		/// <exception cref="ArgumentNullException">One of the arguments provided was null.</exception>
		/// <exception cref="TargetParameterCountException">The binding target has unequal parameters to <paramref name="delegateType"/>.</exception>
		/// <exception cref="TargetParameterNameException">A binding target parameter is not found in the <paramref name="delegateType"/>.</exception>
		/// <exception cref="TargetParameterTypeException">A binding target parameter has a different type than the parameter found in the <paramref name="delegateType"/>.</exception>
		/// <exception cref="TargetParameterAccessException">A binding target parameter has a different access (in/out/ref) than the parameter found in the <paramref name="delegateType"/>.</exception>
		public static Delegate CreateMethod(Type delegateType, Type type, string name, BindingFlags flags = BindingFlags.Public | BindingFlags.Static) =>
			CreateMethod(delegateType, GetMethodInfo(type, name, flags), null);

		/// <summary>
		/// Generates a delegate bound to a named static method in the type.
		/// </summary>
		/// <param name="delegateType">The delegate to bind to the target.</param>
		/// <param name="type">Type the static bind target is in.</param>
		/// <param name="name">Name of the bind target.</param>
		/// <param name="flags">The binding flags to search methods by.</param>
		/// <returns>A delegate to be used for fast-latebound invocation.</returns>
		/// <exception cref="ArgumentNullException">One of the arguments provided was null.</exception>
		/// <exception cref="TargetParameterCountException">The binding target has more parameters than <paramref name="delegateType"/>.</exception>
		/// <exception cref="TargetParameterNameException">A binding target parameter is not found in the <paramref name="delegateType"/>.</exception>
		/// <exception cref="TargetParameterTypeException">A binding target parameter has a different type than the parameter found in the <paramref name="delegateType"/>.</exception>
		/// <exception cref="TargetParameterAccessException">A binding target parameter has a different access (in/out/ref) than the parameter found in the <paramref name="delegateType"/>.</exception>
		public static Delegate CreateMethodLossy(Type delegateType, Type type, string name, BindingFlags flags = BindingFlags.Public | BindingFlags.Static) =>
			CreateMethodLossy(delegateType, GetMethodInfo(type, name, flags), null);

		/// <summary>
		/// Generates a delegate bound to a named method in the instance.
		/// </summary>
		/// <param name="type">Type the static bind target is in.</param>
		/// <param name="name">Name of the bind target.</param>
		/// <param name="flags">The binding flags to search methods by.</param>
		/// <returns>A delegate to be used for fast-latebound invocation.</returns>
		/// <exception cref="ArgumentNullException">One of the arguments provided was null.</exception>
		/// <exception cref="TargetParameterCountException">The binding target has unequal parameters to <typeparamref name="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterNameException">A binding target parameter is not found in the delegate type.</exception>
		/// <exception cref="TargetParameterTypeException">A binding target parameter has a different type than the parameter found in the delegate type.</exception>
		/// <exception cref="TargetParameterAccessException">A binding target parameter has a different access (in/out/ref) than the parameter found in the delegate type.</exception>
		public static TDelegate CreateMethod<TDelegate>(Type type, string name, BindingFlags flags = BindingFlags.Public | BindingFlags.Static) where TDelegate : Delegate =>
			CreateMethod<TDelegate>(GetMethodInfo(type, name, flags), null);

		/// <summary>
		/// Generates a delegate bound to a named method in the instance.
		/// </summary>
		/// <param name="type">Type the static bind target is in.</param>
		/// <param name="name">Name of the bind target.</param>
		/// <param name="flags">The binding flags to search methods by.</param>
		/// <returns>A delegate to be used for fast-latebound invocation.</returns>
		/// <exception cref="ArgumentNullException">One of the arguments provided was null.</exception>
		/// <exception cref="TargetParameterCountException">The binding target has more parameters than <typeparamref name="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterNameException">A binding target parameter is not found in the delegate type.</exception>
		/// <exception cref="TargetParameterTypeException">A binding target parameter has a different type than the parameter found in the delegate type.</exception>
		/// <exception cref="TargetParameterAccessException">A binding target parameter has a different access (in/out/ref) than the parameter found in the delegate type.</exception>
		public static TDelegate CreateMethodLossy<TDelegate>(Type type, string name, BindingFlags flags = BindingFlags.Public | BindingFlags.Static) where TDelegate : Delegate =>
			CreateMethodLossy<TDelegate>(GetMethodInfo(type, name, flags), null);

		#endregion

		#region Any

		/// <summary>
		/// Binds to a method and allows for lossy (not used) parameters with little overhead.
		/// </summary>
		/// <param name="delegateType">The delegate to bind to the target.</param>
		/// <param name="target">The method to bind to.</param>
		/// <param name="instance">The instance the target method is in (null if static).</param>
		/// <returns>Returns a delegate that represents a late-bound method.</returns>
		/// <exception cref="ArgumentNullException">Any of the parameters are null (except for instance if it is static).</exception>
		/// <exception cref="ArgumentException">The type parameter <paramref cref="delegateType"/> is not a valid <see cref="Delegate"/> type.</exception>
		/// <exception cref="TargetReturnException">The return type of <paramref cref="delegateType"/> mismatches with the return type of <see cref="target"/>.</exception>
		/// <exception cref="TargetParameterCountException"><paramref name="target"/> has more parameters than <paramref cref="delegateType"/>.</exception>
		/// <exception cref="TargetParameterNameException"><paramref name="target"/> has a parameter not found in <paramref cref="delegateType"/>.</exception>
		/// <exception cref="TargetParameterTypeException"><paramref name="target"/> has a parameter found in <paramref cref="delegateType"/>, but it is not the same type.</exception>
		/// <exception cref="TargetParameterAccessException"><paramref name="target"/> has a different access (in/out/ref) than the parameter found in the <paramref cref="delegateType"/>.</exception>
		public static Delegate CreateMethodLossy(Type delegateType, MethodInfo target, object instance)
		{
			if (delegateType == null) throw new ArgumentNullException(nameof(delegateType));
			if (delegateType.IsAssignableFrom(typeof(Delegate))) throw new ArgumentException("Not a valid delegate type.", nameof(delegateType));

			MethodInfo site = GetSite(delegateType, target);
			KeyValuePair<Expression[], ParameterExpression[]> parameters = GenerateArgumentExpressionsLossy(target, site.GetParameters());

			return CreateFinalMethod(delegateType, GenerateBody(target, instance, parameters.Key), parameters.Value);
		}

		/// <summary>
		/// Binds to a method and allows for lossy (not used) parameters with little overhead.
		/// </summary>
		/// <param name="delegateType">The delegate to bind to the target.</param>
		/// <param name="target">The method to bind to.</param>
		/// <param name="instance">The instance the target method is in (null if static).</param>
		/// <returns>Returns a delegate that represents a late-bound method.</returns>
		/// <exception cref="ArgumentNullException">Any of the parameters are null (except for instance if it is static).</exception>
		/// <exception cref="ArgumentException">The type parameter <paramref cref="delegateType"/> is not a valid <see cref="Delegate"/> type.</exception>
		/// <exception cref="TargetReturnException">The return type of <paramref cref="delegateType"/> mismatches with the return type of <see cref="target"/>.</exception>
		/// <exception cref="TargetParameterCountException"><paramref name="target"/> has more parameters than <paramref cref="delegateType"/>.</exception>
		/// <exception cref="TargetParameterNameException"><paramref name="target"/> has a parameter not found in <paramref cref="delegateType"/>.</exception>
		/// <exception cref="TargetParameterTypeException"><paramref name="target"/> has a parameter found in <paramref cref="delegateType"/>, but it is not the same type.</exception>
		/// <exception cref="TargetParameterAccessException"><paramref name="target"/> has a different access (in/out/ref) than the parameter found in the <paramref cref="delegateType"/>.</exception>
		public static Delegate CreateMethod(Type delegateType, MethodInfo target, object instance)
		{
			if (delegateType == null) throw new ArgumentNullException(nameof(delegateType));
			if (delegateType.IsAssignableFrom(typeof(Delegate))) throw new ArgumentException("Not a valid delegate type.", nameof(delegateType));

			MethodInfo site = GetSite(delegateType, target);
			ParameterExpression[] parameters = GenerateArgumentExpressions(target, site.GetParameters());

			return CreateFinalMethod(delegateType, GenerateBody(target, instance, parameters), parameters);
		}

		/// <summary>
		/// Binds to a method and allows for lossy (not used) parameters with little overhead.
		/// </summary>
		/// <param name="delegateType">The delegate to bind to the target.</param>
		/// <param name="target">The method to bind to.</param>
		/// <param name="instance">The instance the target method is in (null if static).</param>
		/// <param name="exceptionHandler">The code to run when the catch-block is run.</param>
		/// <returns>Returns a delegate that represents a late-bound method.</returns>
		/// <exception cref="ArgumentNullException">Any of the parameters are null (except for instance if it is static).</exception>
		/// <exception cref="ArgumentException">The type parameter <paramref cref="delegateType"/> is not a valid <see cref="Delegate"/> type.</exception>
		/// <exception cref="TargetReturnException">The return type of <paramref cref="delegateType"/> mismatches with the return type of <see cref="target"/>.</exception>
		/// <exception cref="TargetParameterCountException"><paramref name="target"/> has more parameters than <paramref cref="delegateType"/>.</exception>
		/// <exception cref="TargetParameterNameException"><paramref name="target"/> has a parameter not found in <paramref cref="delegateType"/>.</exception>
		/// <exception cref="TargetParameterTypeException"><paramref name="target"/> has a parameter found in <paramref cref="delegateType"/>, but it is not the same type.</exception>
		/// <exception cref="TargetParameterAccessException"><paramref name="target"/> has a different access (in/out/ref) than the parameter found in the <paramref cref="delegateType"/>.</exception>
		public static Delegate CreateMethodLossy(Type delegateType, MethodInfo target, object instance, Action<Exception> exceptionHandler)
		{
			if (delegateType == null) throw new ArgumentNullException(nameof(delegateType));
			if (delegateType.IsAssignableFrom(typeof(Delegate))) throw new ArgumentException("Not a valid delegate type.", nameof(delegateType));

			MethodInfo site = GetSite(delegateType, target);
			KeyValuePair<Expression[], ParameterExpression[]> parameters = GenerateArgumentExpressionsLossy(target, site.GetParameters());

			return CreateFinalMethod(delegateType, GenerateTryCatchBody(GenerateBody(target, instance, parameters.Key), exceptionHandler), parameters.Value);
		}

		/// <summary>
		/// Binds to a method and allows for lossy (not used) parameters with little overhead.
		/// </summary>
		/// <param name="delegateType">The delegate to bind to the target.</param>
		/// <param name="target">The method to bind to.</param>
		/// <param name="instance">The instance the target method is in (null if static).</param>
		/// <param name="exceptionHandler">The code to run when the catch-block is run.</param>
		/// <returns>Returns a delegate that represents a late-bound method.</returns>
		/// <exception cref="ArgumentNullException">Any of the parameters are null (except for instance if it is static).</exception>
		/// <exception cref="ArgumentException">The type parameter <paramref cref="delegateType"/> is not a valid <see cref="Delegate"/> type.</exception>
		/// <exception cref="TargetReturnException">The return type of <paramref cref="delegateType"/> mismatches with the return type of <see cref="target"/>.</exception>
		/// <exception cref="TargetParameterCountException"><paramref name="target"/> has more parameters than <paramref cref="delegateType"/>.</exception>
		/// <exception cref="TargetParameterNameException"><paramref name="target"/> has a parameter not found in <paramref cref="delegateType"/>.</exception>
		/// <exception cref="TargetParameterTypeException"><paramref name="target"/> has a parameter found in <paramref cref="delegateType"/>, but it is not the same type.</exception>
		/// <exception cref="TargetParameterAccessException"><paramref name="target"/> has a different access (in/out/ref) than the parameter found in the <paramref cref="delegateType"/>.</exception>
		public static Delegate CreateMethod(Type delegateType, MethodInfo target, object instance, Action<Exception> exceptionHandler)
		{
			if (delegateType == null) throw new ArgumentNullException(nameof(delegateType));
			if (delegateType.IsAssignableFrom(typeof(Delegate))) throw new ArgumentException("Not a valid delegate type.", nameof(delegateType));

			MethodInfo site = GetSite(delegateType, target);
			ParameterExpression[] parameters = GenerateArgumentExpressions(target, site.GetParameters());

			return CreateFinalMethod(delegateType, GenerateTryCatchBody(GenerateBody(target, instance, parameters), exceptionHandler), parameters);
		}

		/// <summary>
		/// Binds to a method and allows for lossy (not used) parameters with little overhead.
		/// </summary>
		/// <param name="target">The method to bind to.</param>
		/// <param name="instance">The instance the target method is in (null if static).</param>
		/// <typeparam name="TDelegate">The delegate to bind to the target.</typeparam>
		/// <returns>Returns a delegate that represents a late-bound method.</returns>
		/// <exception cref="ArgumentNullException">Any of the parameters are null (except for instance if it is static).</exception>
		/// <exception cref="TargetReturnException">The return type of <see cref="target"/> mismatches with the return type of <typeparamref cref="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterCountException"><paramref name="target"/> has more parameters than <typeparamref cref="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterNameException"><paramref name="target"/> has a parameter not found in <typeparamref cref="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterTypeException"><paramref name="target"/> has a parameter found in <typeparamref cref="TDelegate"/>, but it is not the same type.</exception>
		/// <exception cref="TargetParameterAccessException"><paramref name="target"/> has a different access (in/out/ref) than the parameter found in the <typeparamref cref="TDelegate"/>.</exception>
		public static TDelegate CreateMethodLossy<TDelegate>(MethodInfo target, object instance) where TDelegate : Delegate
		{
			MethodInfo site = GetSite(typeof(TDelegate), target);
			KeyValuePair<Expression[], ParameterExpression[]> parameters = GenerateArgumentExpressionsLossy(target, site.GetParameters());

			return CreateFinalMethod<TDelegate>(GenerateBody(target, instance, parameters.Key), parameters.Value);
		}

		/// <summary>
		/// Binds to a method and allows for lossy (not used) parameters with little overhead.
		/// </summary>
		/// <param name="target">The method to bind to.</param>
		/// <param name="instance">The instance the target method is in (null if static).</param>
		/// <typeparam name="TDelegate">The delegate to bind to the target.</typeparam>
		/// <returns>Returns a delegate that represents a late-bound method.</returns>
		/// <exception cref="ArgumentNullException">Any of the parameters are null (except for instance if it is static).</exception>
		/// <exception cref="TargetReturnException">The return type of <see cref="target"/> mismatches with the return type of <typeparamref cref="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterCountException"><paramref name="target"/> has unequal parameters to <typeparamref cref="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterNameException"><paramref name="target"/> has a parameter not found in <typeparamref cref="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterTypeException"><paramref name="target"/> has a parameter found in <typeparamref cref="TDelegate"/>, but it is not the same type.</exception>
		/// <exception cref="TargetParameterAccessException"><paramref name="target"/> has a different access (in/out/ref) than the parameter found in the <typeparamref cref="TDelegate"/>.</exception>
		public static TDelegate CreateMethod<TDelegate>(MethodInfo target, object instance) where TDelegate : Delegate
		{
			MethodInfo site = GetSite(typeof(TDelegate), target);
			ParameterExpression[] parameters = GenerateArgumentExpressions(target, site.GetParameters());

			return CreateFinalMethod<TDelegate>(GenerateBody(target, instance, parameters), parameters);
		}

		/// <summary>
		/// Binds to a method and allows for lossy (not used) parameters with little overhead.
		/// </summary>
		/// <param name="target">The method to bind to.</param>
		/// <param name="instance">The instance the target method is in (null if static).</param>
		/// <typeparam name="TDelegate">The delegate to bind to the target.</typeparam>
		/// <param name="exceptionHandler">The code to run when the catch-block is run.</param>
		/// <returns>Returns a delegate that represents a late-bound method.</returns>
		/// <exception cref="ArgumentNullException">Any of the parameters are null (except for instance if it is static).</exception>
		/// <exception cref="TargetReturnException">The return type of <typeparamref name="TDelegate"/> mismatches with the return type of <see cref="target"/>.</exception>
		/// <exception cref="TargetParameterCountException"><paramref name="target"/> has more parameters than <typeparamref cref="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterNameException"><paramref name="target"/> has a parameter not found in <typeparamref cref="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterTypeException"><paramref name="target"/> has a parameter found in <typeparamref cref="TDelegate"/>, but it is not the same type.</exception>
		/// <exception cref="TargetParameterAccessException"><paramref name="target"/> has a different access (in/out/ref) than the parameter found in the <typeparamref cref="TDelegate"/>.</exception>
		public static TDelegate CreateMethodLossy<TDelegate>(MethodInfo target, object instance, Action<Exception> exceptionHandler) where TDelegate : Delegate
		{
			MethodInfo site = GetSite(typeof(TDelegate), target);
			KeyValuePair<Expression[], ParameterExpression[]> parameters = GenerateArgumentExpressionsLossy(target, site.GetParameters());

			return CreateFinalMethod<TDelegate>(GenerateTryCatchBody(GenerateBody(target, instance, parameters.Key), exceptionHandler), parameters.Value);
		}

		/// <summary>
		/// Binds to a method and allows for lossy (not used) parameters with little overhead.
		/// </summary>
		/// <param name="target">The method to bind to.</param>
		/// <param name="instance">The instance the target method is in (null if static).</param>
		/// <typeparam name="TDelegate">The delegate to bind to the target.</typeparam>
		/// <param name="exceptionHandler">The code to run when the catch-block is run.</param>
		/// <returns>Returns a delegate that represents a late-bound method.</returns>
		/// <exception cref="ArgumentNullException">Any of the parameters are null (except for instance if it is static).</exception>
		/// <exception cref="TargetReturnException">The return type of <typeparamref name="TDelegate"/> mismatches with the return type of <see cref="target"/>.</exception>
		/// <exception cref="TargetParameterCountException"><paramref name="target"/> has unequal parameters to <typeparamref cref="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterNameException"><paramref name="target"/> has a parameter not found in <typeparamref cref="TDelegate"/>.</exception>
		/// <exception cref="TargetParameterTypeException"><paramref name="target"/> has a parameter found in <typeparamref cref="TDelegate"/>, but it is not the same type.</exception>
		/// <exception cref="TargetParameterAccessException"><paramref name="target"/> has a different access (in/out/ref) than the parameter found in the <typeparamref cref="TDelegate"/>.</exception>
		public static TDelegate CreateMethod<TDelegate>(MethodInfo target, object instance, Action<Exception> exceptionHandler) where TDelegate : Delegate
		{
			MethodInfo site = GetSite(typeof(TDelegate), target);
			KeyValuePair<Expression[], ParameterExpression[]> parameters = GenerateArgumentExpressionsLossy(target, site.GetParameters());

			return CreateFinalMethod<TDelegate>(GenerateTryCatchBody(GenerateBody(target, instance, parameters.Key), exceptionHandler), parameters.Value);
		}

		#endregion

		#endregion

		#region Info

		private static MethodInfo GetMethodInfo(object instance, string name, BindingFlags flags)
		{
			if (instance == null) throw new ArgumentNullException(nameof(instance));

			return GetMethodInfo(instance.GetType(), name, flags);
		}

		private static MethodInfo GetMethodInfo(Type type, string name, BindingFlags flags)
		{
			if (type == null) throw new ArgumentNullException(nameof(type));
			if (name == null) throw new ArgumentNullException(nameof(name));

			MethodInfo target = type.GetMethod(name, flags);
			if (target == null) throw new MissingMethodException($"No matching method ({name} in {type}) was found.");

			return target;
		}

		private static MethodInfo GetSite(Type delegateType, MethodInfo target)
		{
			if (delegateType == null) throw new ArgumentNullException(nameof(delegateType));
			if (target == null) throw new ArgumentNullException(nameof(target));

			const string invokeMethod = "Invoke";
			// Should be public instance type, but just in case(tm)
			MethodInfo site = delegateType.GetMethod(invokeMethod);
			// ReSharper disable once PossibleNullReferenceException	All hell would have broken loose
			if (site.ReturnType != target.ReturnType) throw new TargetReturnException($"The return type of the target ({target.ReturnType}) does not match with that of the delegate ({site.ReturnType}).");

			return site;
		}

		#endregion

		#region Generation

		private static Expression GenerateTryCatchBody(Expression body, Action<Exception> exceptionHandler)
		{
			if (body == null) throw new ArgumentNullException(nameof(body));
			if (exceptionHandler == null) throw new ArgumentNullException(nameof(exceptionHandler));

			ParameterExpression e = Expression.Parameter(typeof(Exception), "e");

			return Expression.TryCatch(
				body,
				Expression.Catch(
					e,
					Expression.Call(
						Expression.Constant(exceptionHandler.Target),
						exceptionHandler.Method,
						e
					)
				)
			);
		}

		private static Expression GenerateBody(MethodInfo target, object instance, IEnumerable<Expression> arguments)
		{
			if (target == null) throw new ArgumentNullException(nameof(target));
			if (arguments == null) throw new ArgumentNullException(nameof(arguments));

			if (target.IsStatic)
			{
				return CreateBody(target, arguments);
			}

			if (instance == null) throw new ArgumentNullException(nameof(instance), "An instance is required for a non-static method to be bound to.");

			return CreateBody(target, instance, arguments);
		}

		private static KeyValuePair<Expression[], ParameterExpression[]> GenerateArgumentExpressionsLossy(MethodInfo target, IReadOnlyList<ParameterInfo> outerParameters)
		{
			if (target == null) throw new ArgumentNullException(nameof(target));
			if (outerParameters == null) throw new ArgumentNullException(nameof(outerParameters));

			ParameterInfo[] innerParameters = target.GetParameters();

			if (outerParameters.Count < innerParameters.Length) throw new TargetParameterCountException("The target method contains more parameters than those that are passed.");

			// Dictionary of all strings and their parameter INFOs
			Dictionary<string, ParameterInfo> outerInfoMap = outerParameters.ToDictionary(x => x.Name, x => x);
			// Dictionary of all strings and their parameter EXPRESSIONs
			Expression[] innerArguments = new Expression[innerParameters.Length];
			Dictionary<ParameterInfo, ParameterExpression> innerArgumentsMap = new Dictionary<ParameterInfo, ParameterExpression>(innerArguments.Length);

			// Creates inner parameter expressions from the infos if they are one of the allowed parameters.
			for (int i = 0; i < innerArguments.Length; i++)
			{
				ParameterInfo innerInfo = innerParameters[i];

				if (outerInfoMap.TryGetValue(innerInfo.Name, out ParameterInfo outerInfo))
				{
					if (outerInfo.IsIn != innerInfo.IsIn || outerInfo.IsOut != innerInfo.IsOut || (!outerInfo.ParameterType.IsByRef && innerInfo.ParameterType.IsByRef)) throw new TargetParameterAccessException("The target method had a known parameter, but a mismatched access (in/out).");

					Type outerNonRef = outerInfo.ParameterType;
					string outerName = outerNonRef.FullName;
					// ReSharper disable once PossibleNullReferenceException	it can't be null tho
					if (outerNonRef.IsByRef) outerNonRef = Type.GetType(outerName.Substring(0, outerName.Length - 1));

					Type innerNonRef = innerInfo.ParameterType;
					string innerName = innerNonRef.FullName;
					// ReSharper disable once PossibleNullReferenceException	reee it can't be null tho
					if (innerNonRef.IsByRef) innerNonRef = Type.GetType(innerName.Substring(0, innerName.Length - 1));

					if (outerNonRef != innerNonRef) throw new TargetParameterTypeException($"The target method had a known parameter ({innerInfo.Name}), but a mismatched type.");

					ParameterExpression expression = Expression.Parameter(outerInfo.ParameterType, outerInfo.Name);
					innerArguments[i] = expression;
					innerArgumentsMap.Add(outerInfo, expression);
				}
				else
				{
					throw new TargetParameterNameException("The target method had an unknown parameter.");
				}
			}

			// Creates outer parameter expressions from the passed parameters.
			ParameterExpression[] outerArguments = new ParameterExpression[outerParameters.Count];
			for (int i = 0; i < outerArguments.Length; i++)
			{
				ParameterInfo outerInfo = outerParameters[i];

				if (innerArgumentsMap.TryGetValue(outerInfo, out ParameterExpression innerArgument)) // Use the preexisting parameter.
				{
					outerArguments[i] = innerArgument;
				}
				else // Create a new parameter that goes nowhere for compatibility.
				{
					outerArguments[i] = Expression.Parameter(outerInfo.ParameterType, outerInfo.Name);
				}
			}

			return new KeyValuePair<Expression[], ParameterExpression[]>(innerArguments, outerArguments);
		}

		private static ParameterExpression[] GenerateArgumentExpressions(MethodInfo target, IReadOnlyList<ParameterInfo> outerParameters)
		{
			if (target == null) throw new ArgumentNullException(nameof(target));
			if (outerParameters == null) throw new ArgumentNullException(nameof(outerParameters));

			ParameterInfo[] innerParameters = target.GetParameters();

			if (innerParameters.Length != outerParameters.Count) throw new TargetParameterCountException("The target method contains a different number of parameters than those that are passed.");

			ParameterExpression[] parameters = new ParameterExpression[outerParameters.Count];
			for (int i = 0; i < outerParameters.Count; i++)
			{
				ParameterInfo outerParameter = outerParameters[i];
				ParameterInfo innerParameter = innerParameters[i];

				if (outerParameter.Name != innerParameter.Name) throw new TargetParameterNameException("The target method had an unknown parameter.");
				if (outerParameter.IsIn != innerParameter.IsIn ||
				    outerParameter.IsOut != innerParameter.IsOut ||
				    outerParameter.ParameterType.IsByRef != innerParameter.ParameterType.IsByRef) throw new TargetParameterAccessException("The target method had a known parameter, but a mismatched access (in/out).");
				if (outerParameter.ParameterType != innerParameter.ParameterType) throw new TargetParameterTypeException("The target method had a known parameter, but a mismatched type.");

				parameters[i] = Expression.Parameter(outerParameter.ParameterType, outerParameter.Name);
			}

			return parameters;
		}

		#endregion

		#region Finalization

		private static Expression CreateBody(MethodInfo target, object instance, IEnumerable<Expression> arguments) =>
			Expression.Call(
				Expression.Constant(instance),
				target,
				arguments
			);

		private static Expression CreateBody(MethodInfo target, IEnumerable<Expression> arguments) =>
			Expression.Call(
				target,
				arguments
			);

		private static TDelegate CreateFinalMethod<TDelegate>(Expression body, IEnumerable<ParameterExpression> parameters) where TDelegate : Delegate =>
			Expression.Lambda<TDelegate>(
				body,
				parameters
			).Compile();


		private static Delegate CreateFinalMethod(Type delegateType, Expression body, IEnumerable<ParameterExpression> parameters) =>
			Expression.Lambda(
				delegateType,
				body,
				parameters
			).Compile();

		#endregion
	}
}
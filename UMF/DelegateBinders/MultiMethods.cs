using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace UMF.DelegateBinders
{
	public partial class ReflectionHelpers
	{
		#region Foreaching

		private static Expression ForBody(ParameterExpression i, Expression lower, Expression upper, Expression body)
		{
			LabelTarget exit = Expression.Label(typeof(void));
			return Expression.Block(
				new[] {i},
				Expression.Assign(
					i,
					lower
				),
				Expression.Loop(
					Expression.Block(
						Expression.IfThenElse(
							Expression.LessThan(i, upper),
							body,
							Expression.Break(exit)
						),
						Expression.AddAssign(i, Expression.Constant(1))
					),
					exit
				)
			);
		}

		private static KeyValuePair<ParameterExpression[], Expression> GetForEachInfo(Type delegateType, IReadOnlyCollection<Delegate> delegates)
		{
			MethodInfo site = delegateType.GetMethod("Invoke");
			// ReSharper disable once PossibleNullReferenceException
			ParameterInfo[] parameterInfos = site.GetParameters();
			ParameterExpression[] outerParameters = new ParameterExpression[parameterInfos.Length];
			Expression[] innerParameters = new Expression[outerParameters.Length];

			for (int j = 0; j < parameterInfos.Length; j++)
			{
				ParameterInfo parameter = parameterInfos[j];

				outerParameters[j] = Expression.Parameter(parameter.ParameterType, parameter.Name);
				innerParameters[j] = outerParameters[j];
			}

			ParameterExpression i = Expression.Parameter(typeof(int), "i");
			return new KeyValuePair<ParameterExpression[], Expression>(outerParameters, ForBody(
				i,
				Expression.Constant(0),
				Expression.Constant(delegates.Count),
				Expression.Call(
					Expression.Convert(
						Expression.ArrayAccess(
							Expression.Constant(delegates),
							i
						),
						delegateType
					),
					site,
					innerParameters
				)
			));
		}

		#endregion

		/// <summary>
		/// Collects a group of delegates and places them under another delegate which foreaches and passes all the arguments.
		/// </summary>
		/// <param name="delegates">Delegates to group.</param>
		/// <typeparam name="TDelegate">Type of the delegates.</typeparam>
		/// <returns>A delegate which invokes all the grouped delegates.</returns>
		public static TDelegate CreateMutliMethod<TDelegate>(TDelegate[] delegates) where TDelegate : Delegate
		{
			KeyValuePair<ParameterExpression[], Expression> forLoop = GetForEachInfo(typeof(TDelegate), delegates);

			return Expression.Lambda<TDelegate>(
				forLoop.Value,
				forLoop.Key
			).Compile();
		}

		/// <summary>
		/// Collects a group of delegates and places them under another delegate which foreaches and passes all the arguments.
		/// </summary>
		/// <param name="delegateType">Type of the delegates.</param>
		/// <param name="delegates">Delegates to group.</param>
		/// <returns>A delegate which invokes all the grouped delegates.</returns>
		public static Delegate CreateMutliMethod(Type delegateType, Delegate[] delegates)
		{
			foreach (Delegate itemDelegate in delegates)
			{
				if (itemDelegate.GetType() != delegateType) throw new ArgumentException("A delegate was not the proper type.", nameof(delegates));
			}

			KeyValuePair<ParameterExpression[], Expression> forLoop = GetForEachInfo(delegateType, delegates);

			return Expression.Lambda(
				delegateType,
				forLoop.Value,
				forLoop.Key
			).Compile();
		}
	}
}
using System.Reflection;

namespace UMF.DelegateBinders.Exceptions
{
	/// <inheritdoc />
	/// <summary>
	/// The exception that is thrown when a parameter of the same name cannot be found.
	/// </summary>
	public class TargetParameterNameException : TargetException
	{
		/// <inheritdoc />
		public TargetParameterNameException(string message) : base(message) { }
	}
}
using System.Reflection;

namespace UMF.DelegateBinders.Exceptions
{
	/// <inheritdoc />
	/// <summary>
	/// The exception that is thrown when the parameter of the same name does not have the same type.
	/// </summary>
	public class TargetParameterTypeException : TargetException
	{
		/// <inheritdoc />
		public TargetParameterTypeException(string message) : base(message) { }
	}
}
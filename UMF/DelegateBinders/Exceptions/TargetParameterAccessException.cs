using System.Reflection;

namespace UMF.DelegateBinders.Exceptions
{
	/// <inheritdoc />
	/// <summary>
	/// The exception that is thrown when a parameter has an invalid access.
	/// </summary>
	public class TargetParameterAccessException : TargetException
	{
		/// <inheritdoc />
		public TargetParameterAccessException(string message) : base(message) { }
	}
}
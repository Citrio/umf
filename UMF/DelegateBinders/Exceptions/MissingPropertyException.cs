using System;

namespace UMF.DelegateBinders.Exceptions
{
	/// <inheritdoc />
	/// <summary>
	/// The exception that is thrown when a property cannot be found.
	/// </summary>
	public class MissingPropertyException : MissingMemberException
	{
		/// <inheritdoc />
		public MissingPropertyException(string message) : base(message) { }
	}
}
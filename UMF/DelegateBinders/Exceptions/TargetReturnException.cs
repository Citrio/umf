using System.Reflection;

namespace UMF.DelegateBinders.Exceptions
{
	/// <inheritdoc />
	/// <summary>
	/// The exception that is thrown when the target method does not have the same return type.
	/// </summary>
	public class TargetReturnException : TargetException
	{
		/// <inheritdoc />
		public TargetReturnException(string message) : base(message) { }
	}
}
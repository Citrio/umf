using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UMF.Core;
using UMF.Core.Log;
using UMF.DelegateBinders;
using UMF.Mods;
using UMF.Settings;

namespace UMF.Events
{
	/// <inheritdoc />
	/// <summary>
	/// The module for hooking and invocating events.
	/// </summary>
	public sealed class EventModule : UmfModule
	{
		/// <summary>
		/// The single instance of <see cref="EventModule"/>.
		/// </summary>
		public static EventModule Instance { get; private set; }

		/// <inheritdoc />
		public override string SettingsId { get; } = "Events";

		private static void Init()
		{
			if (Instance != null) throw new InvalidOperationException("Already instantiated.");

			Instance = new EventModule();
		}

		private Dictionary<Type, UmfEvent> activeHooks;
		private Dictionary<Type, Delegate> activeEvents;

		private EventModule()
		{
			activeHooks = new Dictionary<Type, UmfEvent>();
			activeEvents = new Dictionary<Type, Delegate>();
		}

		private static bool TryBind(UmfMod mod, IEnumerable<MethodInfo> methods, Type delegateType, out Delegate bound, out MethodInfo boundTarget)
		{
			IEnumerable<MethodInfo> targets =
				methods
					.Where(x => x.Name == delegateType.Name)
					.OrderByDescending(x => x.GetParameters().Length);

			Delegate eventHandler = null;
			boundTarget = null;
			foreach (MethodInfo target in targets)
			{
				Logger.UmfDebug(nameof(EventModule), $"Attempting to bind {target.Name} to {delegateType.Name}...");

				boundTarget = target;
				try
				{
					eventHandler = ReflectionHelpers.CreateMethodLossy(
						delegateType,
						target,
						mod,
						e => Logger.Error(nameof(EventModule), $"{mod} threw an unhandled exception while calling {target}:\n{e}")
					);

					break;
				}
				catch (Exception e)
				{
					if (target.GetCustomAttribute<NonCore>() == null)
					{
						Logger.Error(nameof(EventModule), $"Failed to bind {boundTarget}. {mod} will be unloaded. Full exception:\n{e}");
						bound = null;
						return false;
					}

					Logger.Warning(nameof(EventModule), $"Failed to bind {boundTarget}, however {mod} will proceed because the event is marked with the {nameof(NonCore)} attribute. Full exception:\n{e}");

					boundTarget = null;
				}
			}

			bound = eventHandler;
			return true;
		}

		private void UnhookAll()
		{
			foreach (UmfEvent hook in activeHooks.Values)
			{
				hook.Unhook();
			}

			activeHooks.Clear();
			activeEvents.Clear();
		}

		private void HookGame(IEnumerable<Type> hookTypes, out Dictionary<Type, UmfEvent> hooks)
		{
			hooks = new Dictionary<Type, UmfEvent>();

			foreach (Type hookType in hookTypes)
			{
				if (!typeof(UmfEvent).IsAssignableFrom(hookType) || hookType.IsAbstract) continue;

				UmfEvent hook = (UmfEvent) Activator.CreateInstance(hookType);
				hook.Hook();
				Logger.UmfDebug(nameof(EventModule), $"Hooked event {hook.Delegate.Name}.");

				hooks.Add(hookType, hook);
			}
		}

		private void HookMods(IReadOnlyCollection<Type> events, out Dictionary<Type, Delegate> finalEvents)
		{
			Dictionary<Type, Delegate> modEvents = new Dictionary<Type, Delegate>();
			Dictionary<Type, Dictionary<UmfMod, Delegate>> allModEvents = events.ToDictionary(x => x, x => new Dictionary<UmfMod, Delegate>());
			finalEvents = new Dictionary<Type, Delegate>();

			List<UmfMod> toUnload = new List<UmfMod>();
			foreach (UmfMod mod in LoadedMods)
			{
				MethodInfo[] methods = mod.GetType().GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
				bool success = true;

				foreach (Type eventDelegate in events)
				{
					if (!TryBind(mod, methods, eventDelegate, out Delegate bound, out MethodInfo boundTarget))
					{
						toUnload.Add(mod);
						success = false;
						break;
					}

					if (bound != null)
					{
						Logger.UmfDebug(nameof(EventModule), $"Bound {boundTarget} successfully.");
						modEvents.Add(eventDelegate, bound);
					}
				}

				if (!success) continue;

				foreach (KeyValuePair<Type, Delegate> modEvent in modEvents)
				{
					allModEvents[modEvent.Key].Add(mod, modEvent.Value);
				}

				modEvents.Clear();

				Logger.UmfDebug(nameof(EventModule), $"Hooked {mod} completely.");
			}

			foreach (UmfMod mod in toUnload)
			{
				UmfCore.Instance.Unload(mod);
			}

			foreach (KeyValuePair<Type, Dictionary<UmfMod, Delegate>> hookedDelegate in allModEvents)
			{
				switch (hookedDelegate.Value.Count)
				{
					case 0:
						continue;

					case 1:
					{
						finalEvents.Add(hookedDelegate.Key, hookedDelegate.Value.Values.First());

						break;
					}

					default:
					{
						finalEvents.Add(
							hookedDelegate.Key,
							ReflectionHelpers.CreateMutliMethod(
								hookedDelegate.Key,
								hookedDelegate
									.Value.OrderBy(
										x =>
											x.Key.ModSettings.events != null &&
											x.Key.ModSettings.events.TryGetValue(hookedDelegate.Key.Name, out UmfModEventSettings settings)
												? settings
												: null
									)
									.Select(x => x.Value)
									.ToArray()
							)
						);
						break;
					}
				}
			}
		}

		/// <summary>
		/// Rehooks every event to the game and rehooks every mod to events.
		/// <para>THIS IS EXTREMELY EXPENSIVE AND NOT RECOMMENDED TO CALL. <code>ONLY CALL THIS IF YOU LOAD AN ASSEMBLY WITH HOOKABLE EVENTS IN IT</code>.</para>
		/// </summary>
		public void RebuildEvents()
		{
			Logger.UmfDebug(nameof(EventModule), "Rebuilding events...");

			UnhookAll();

			IEnumerable<Type> types = UmfCore.Instance.UmfAssemblies.SelectMany(x =>
			{
				try
				{
					return x.GetTypes();
				}
				catch (ReflectionTypeLoadException e)
				{
					return e.Types;
				}
			});

			HookGame(types, out activeHooks);
			HookMods(activeHooks.Values.Select(x => x.Delegate).ToArray(), out activeEvents);

			Logger.UmfDebug(nameof(EventModule), "Events rebuilt.");
		}

		/// <inheritdoc />
		public override bool Load(UmfMod mod)
		{
			if (!base.Load(mod)) return false;

			if (UmfCore.Initialized) RebuildEvents();

			return true;
		}

		/// <inheritdoc />
		public override bool Unload(UmfMod mod)
		{
			if (!base.Unload(mod)) return false;

			if (UmfCore.Initialized) RebuildEvents();

			return true;
		}

		internal Delegate HandleCalledEvent(Type delegateType, Assembly assembly)
		{
			if (assembly == null) throw new ArgumentNullException(nameof(assembly));
			if (delegateType.Assembly != assembly) throw new InvalidOperationException("Events can only be called by the assemblies that declare them.");

			return
				activeEvents.TryGetValue(delegateType, out Delegate handler)
					? handler
					: null;
		}

		internal TDelegate HandleCalledEvent<TDelegate>(Assembly assembly) where TDelegate : Delegate =>
			(TDelegate) HandleCalledEvent(typeof(TDelegate), assembly);

		/// <summary>
		/// Fetches the delegate that handles and try-catches all subscribed mods.
		/// <para>This may return null if there is no subscription, so be sure to always use the null conditional operator (<code>?.</code>).</para>
		/// </summary>
		/// <param name="delegateType">Type of the event to fetch.</param>
		public Delegate HandleEvent(Type delegateType) =>
			HandleCalledEvent(delegateType, Assembly.GetCallingAssembly());

		/// <summary>
		/// Fetches the delegate that handles and try-catches all subscribed mods.
		/// <para>This may return null if there is no subscription, so be sure to always use the null conditional operator (<code>?.</code>).</para>
		/// </summary>
		/// <typeparam name="TDelegate">Type of the event to fetch.</typeparam>
		public TDelegate HandleEvent<TDelegate>() where TDelegate : Delegate =>
			HandleCalledEvent<TDelegate>(Assembly.GetCallingAssembly());
	}
}
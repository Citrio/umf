using System;

namespace UMF.Events
{
	/// <inheritdoc />
	/// <summary>
	/// Marks an event method as non-core. If this event fails to bind, the mod will not be unloaded.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method)]
	public class NonCore : Attribute { }
}
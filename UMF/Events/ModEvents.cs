using UMF.Mods;

namespace UMF.Events
{
	internal class LoadStartEvent : UmfEvent
	{
		public delegate void OnLoadStart(UmfMod mod);

		public LoadStartEvent() : base(typeof(OnLoadStart)) { }
	}
	
	internal class LoadFinishEvent : UmfEvent
	{
		public delegate void OnLoadFinish(UmfMod mod);

		public LoadFinishEvent() : base(typeof(OnLoadFinish)) { }
	}
	
	internal class UnloadStartEvent : UmfEvent
	{
		public delegate void OnUnloadStart(UmfMod mod);

		public UnloadStartEvent() : base(typeof(OnUnloadStart)) { }
	}
	
	internal class UnloadFinishEvent : UmfEvent
	{
		public delegate void OnUnloadFinish(UmfMod mod);

		public UnloadFinishEvent() : base(typeof(OnUnloadFinish)) { }
	}
}
using System;
using System.Reflection;

namespace UMF.Events
{
	/// <summary>
	/// A broadcastable event for UMF mods.
	/// </summary>
	public abstract class UmfEvent
	{
		/// <summary>
		/// Indicates that all events are locked and should not fire; skip over all event code.
		/// </summary>
		public static bool Locked { get; private set; }

		/// <summary>
		/// Type of the delegate that is bound to the method targets.
		/// </summary>
		public Type Delegate { get; }

		/// <summary>
		/// Creates an instance of <see cref="UmfEvent"/>.
		/// </summary>
		/// <param name="delegateType"><seealso cref="Delegate"/></param>
		protected UmfEvent(Type delegateType)
		{
			Delegate = delegateType ?? throw new ArgumentNullException(nameof(delegateType));
		}

		/// <summary>
		/// Hooks the event to the application. Do invoke events after this.
		/// </summary>
		public virtual void Hook() { }
		/// <summary>
		/// Unhooks the event from the application. Do not invoke events after this.
		/// </summary>
		public virtual void Unhook() { }

		/// <summary>
		/// Locks all event code for the duration of the action.
		/// </summary>
		/// <param name="action">Code to run while all events are locked.</param>
		public static void LockRun(Action action)
		{
			Locked = true;
			action();
			Locked = false;
		}

		/// <summary>
		/// Shortcut to <see cref="EventModule"/>'s generic handle method.
		/// </summary>
		protected static TDelegate Handle<TDelegate>() where TDelegate : Delegate =>
			EventModule.Instance.HandleCalledEvent<TDelegate>(Assembly.GetCallingAssembly());

		/// <summary>
		/// Shortcut to <see cref="EventModule"/>'s parameterized handle method.
		/// </summary>
		protected static Delegate Handle(Type delegateType) =>
			EventModule.Instance.HandleCalledEvent(delegateType, Assembly.GetCallingAssembly());
	}
}
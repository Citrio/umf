using System;
using Harmony;

namespace UMF.Events
{
	/// <inheritdoc />
	/// <summary>
	/// A broadcastable event for UMF mods that is able to Harmony methods.
	/// <para>Usable Harmony methods include.</para>
	/// </summary>
	public abstract class UmfHarmonyEvent : UmfEvent
	{
		private readonly HarmonyInstance harmony;

		/// <summary>
		/// The Harmony patch data for this event.
		/// </summary>
		public UmfPatch Patch { get; }

		/// <inheritdoc />
		/// <summary>
		/// Creates an instance of <see cref="UmfHarmonyEvent"/>.
		/// </summary>
		/// <param name="delegateType"><seealso cref="Delegate"/></param>
		/// <param name="target">The class/struct that contains the method to Harmony patch.</param>
		/// <param name="name">The name of the method to Harmony patch.</param>
		/// <param name="args">The arguments of the method to Harmony patch (only needed if there are overloads).</param>
		protected UmfHarmonyEvent(Type delegateType, Type target, string name, Type[] args = null) : base(delegateType)
		{
			Patch = UmfPatch.Create(
				target,
				name,
				GetType(),
				args
			);

			harmony = HarmonyInstance.Create(GetType().FullName);
		}

		/// <inheritdoc />
		public override void Hook()
		{
			Patch.Patch(harmony);
		}

		/// <inheritdoc />
		public override void Unhook()
		{
			Patch.Unpatch(harmony);
		}
	}
}
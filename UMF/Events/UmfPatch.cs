using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Harmony;
using UMF.Core.Log;

namespace UMF.Events
{
	/// <summary>
	/// The Harmony patch data for a <see cref="UmfHarmonyEvent"/>.
	/// </summary>
	public class UmfPatch
    {
	    /// <summary>
	    /// The method that is being patched.
	    /// </summary>
    	public MethodBase Target { get; }
	    /// <summary>
	    /// The Harmony prefix of the method.
	    /// </summary>
    	public HarmonyMethod Prefix { get; }
	    /// <summary>
	    /// The Harmony postfix of the method.
	    /// </summary>
    	public HarmonyMethod Postfix { get; }
	    /// <summary>
	    /// The Harmony transpiler of the method.
	    /// </summary>
        public HarmonyMethod Transpiler { get; }

	    /// <summary>
	    /// Creates an instance of <see cref="UmfPatch"/>.
	    /// </summary>
	    /// <param name="target"><seealso cref="Target"/></param>
	    /// <param name="prefix"><seealso cref="Prefix"/></param>
	    /// <param name="postfix"><seealso cref="Postfix"/></param>
	    /// <param name="transpiler"><seealso cref="Transpiler"/></param>
	    /// <exception cref="ArgumentNullException"><paramref name="target"/> is null.</exception>
    	public UmfPatch(MethodBase target, HarmonyMethod prefix, HarmonyMethod postfix, HarmonyMethod transpiler)
    	{
    		if (target == null) throw new ArgumentNullException(nameof(target));

    		Target = target;
    		Prefix = prefix;
    		Postfix = postfix;
            Transpiler = transpiler;
        }

    	/// <summary>
    	/// Applies all the Harmony patch data to the target method.
    	/// </summary>
    	/// <param name="harmony">Harmony instance to apply the patch to.</param>
    	public void Patch(HarmonyInstance harmony)
    	{
    		harmony.Patch(Target, Prefix, Postfix, Transpiler);
    	}

    	/// <summary>
    	/// Strips all the Harmony patch data from the target method.
    	/// </summary>
    	/// <param name="harmony">Harmony instance to strip the patch from.</param>
    	public void Unpatch(HarmonyInstance harmony)
    	{
            if (Transpiler != null)
            {
	            harmony.Unpatch(Target, Transpiler.method);
            }

    		if (Postfix != null)
    		{
    			harmony.Unpatch(Target, Postfix.method);
    		}

    		if (Prefix != null)
    		{
    			harmony.Unpatch(Target, Prefix.method);
    		}
    	}

    	private static HarmonyMethod FindHarmonyMethod(Type target, string name)
    	{
    		const BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Static;

            MethodInfo method = target.GetMethod(name, flags);

            if (method == null)
            {
                if (target.GetMethod(name, flags | BindingFlags.Instance) != null)
                {
	                Logger.Error(nameof(UmfHarmonyEvent), $"{target} has an instance, not static, {name}.");
                }

                if (target.GetMethod(name, flags | BindingFlags.Public) != null)
                {
	                Logger.Error(nameof(UmfHarmonyEvent), $"{target} has a public, not private, {name}.");
                }
            }

            return
                method == null
	                ? null
	                : new HarmonyMethod(method);
        }

    	/// <summary>
    	/// Creates a patch to be applied/stripped from a Harmony instance.
    	/// </summary>
    	/// <param name="target">The type that contains the target method.</param>
    	/// <param name="name">The name of the target method.</param>
    	/// <param name="patch">The type that contains the static Harmony methods for use in the patch.</param>
    	/// <param name="args">The optional arguments to help search for the target method. Used if there are overloads.</param>
    	/// <exception cref="ArgumentNullException"><paramref name="target"/>, <paramref name="name"/>, or <paramref name="patch"/> is null.</exception>
    	/// <exception cref="ArgumentException"><paramref name="name"/> is empty or whitespace.</exception>
    	/// <exception cref="MissingMethodException">Failed to find the target method.</exception>
    	public static UmfPatch Create(Type target, string name, Type patch, Type[] args = null)
    	{
    		if (target == null) throw new ArgumentNullException(nameof(target));
    		if (name == null) throw new ArgumentNullException(nameof(name));
            if (patch == null) throw new ArgumentNullException(nameof(patch));
    		if (string.IsNullOrWhiteSpace(name)) throw new ArgumentException(nameof(name));

    		const BindingFlags flagAll = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;
    		MethodInfo targetMethod = args == null ?
    			target.GetMethod(name, flagAll) :
    			target.GetMethod(name, flagAll, null, args, null);

    		if (targetMethod == null) throw new MissingMethodException("Patch target not found.");

            HarmonyMethod transpiler = FindHarmonyMethod(patch, "Transpiler");

            if (transpiler != null)
            {
                Type instructions = typeof(IEnumerable<CodeInstruction>);

                if (transpiler.declaringType != instructions)
                {
	                Logger.Error(nameof(UmfHarmonyEvent), $"{transpiler.method} does not return {instructions}.");
                }

                if (transpiler.argumentTypes.Length == 0 || !transpiler.argumentTypes.Contains(instructions))
                {
	                Logger.Error(nameof(UmfHarmonyEvent), $"{transpiler.method} does not have the mandatory {instructions} \"instructions\" argument.");
                }
            }

    		return new UmfPatch(targetMethod, FindHarmonyMethod(patch, "Prefix"), FindHarmonyMethod(patch, "Postfix"), transpiler);
    	}
    }
}
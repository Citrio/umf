using System;
using System.Linq.Expressions;
using System.Reflection;
using UMF.DelegateBinders;
using UMF.DelegateBinders.Exceptions;

namespace UMF.Core
{
	/// <summary>
	/// Managed way for interacting with a UMF module.
	/// </summary>
	public class ManagedUmfModule
	{
		private readonly Func<UmfModule> instanceGetter;
		/// <summary>
		/// The instance of the module.
		/// </summary>
		public UmfModule Instance => instanceGetter();

		/// <summary>
		/// The static initializer of the module.
		/// </summary>
		public UmfModule.OnInit Init { get; }

		/// <summary>
		/// Creates a managed instance of a module from an unmanaged type of the module.
		/// </summary>
		/// <param name="type">The UMF module to manage.</param>
		/// <exception cref="ArgumentNullException"><paramref name="type"/> is null.</exception>
		/// <exception cref="ArgumentException"><paramref name="type"/> is not a <see cref="UmfModule"/>.</exception>
		public ManagedUmfModule(Type type)
		{
			if (type == null) throw new ArgumentNullException(nameof(type));
			if (!typeof(UmfModule).IsAssignableFrom(type)) throw new ArgumentException("Not a valid module.", nameof(type));

			instanceGetter = CreateInstanceWrapper(type);
			Init = ReflectionHelpers.CreateMethod<UmfModule.OnInit>(type, nameof(Init), BindingFlags.NonPublic | BindingFlags.Static);
		}

		private static Func<UmfModule> CreateInstanceWrapper(Type type)
		{
			const string propertyName = "Instance";
			MethodInfo instanceGetter = type.GetProperty(propertyName)?.GetGetMethod() ?? throw new MissingPropertyException($"{type}.{propertyName}");

			return Expression.Lambda<Func<UmfModule>>(
				Expression.Property(
					null,
					instanceGetter
				)
			).Compile();
		}
	}
}
using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Security;

namespace UMF.Core.Log
{
	/// <summary>
	/// Handles logging messages from UMF framework/mods.
	/// </summary>
	public static class Logger
	{
		/// <summary>
		/// The importance of the log message.
		/// </summary>
		public enum Severity
		{
			/// <summary>
			/// For user's information.
			/// </summary>
			Info,
			/// <summary>
			/// For a non-fatal error or an indicator that a future error may occur.
			/// </summary>
			Warning,
			/// <summary>
			/// For fatal errors only.
			/// </summary>
			Error,
			/// <summary>
			/// For excessive information, always logged to file but opt-in to display to console.
			/// </summary>
			Debug
		}

		private static readonly StreamWriter _file;

		/// <summary>
		/// Relative path to the logs directory.
		/// </summary>
		public const string kLogsDirectory = UmfCore.kUmfDirectory + "logs/";
		/// <summary>
		/// Relative path to the latest log file.
		/// </summary>
		public const string kLatestFile = kLogsDirectory + "latest.log";

		static Logger()
		{
			if (Directory.Exists(kLogsDirectory))
			{
				if (File.Exists(kLatestFile))
				{
					CompressLatest();
				}

				_file = new StreamWriter(kLatestFile)
				{
					AutoFlush = true
				};
			}
		}

		private static void CompressLatest()
		{
			UmfDebug(nameof(Logger), "Compressing previous log file...");

			Stopwatch sw = Stopwatch.StartNew();

			FileStream latest;
			try
			{
				latest = new FileStream(kLatestFile, FileMode.Open, FileAccess.Read);
			}
			catch (UnauthorizedAccessException)
			{
				Error(nameof(Logger), "Failed to open latest log for reading: unauthorized to read.");
				return;
			}
			catch (SecurityException)
			{
				Error(nameof(Logger), "Failed to open latest log for reading: missing permissions.");
				return;
			}

			FileStream compressed;
			try
			{
				compressed = new FileStream($"{kLogsDirectory}{File.GetCreationTimeUtc(kLatestFile):yyyy-MM-dd hh-mm-ss}.log.gz", FileMode.Create, FileAccess.Write, FileShare.None);
			}
			catch (UnauthorizedAccessException)
			{
				Error(nameof(Logger), "Failed to open compressed log for writing: unauthorized to read.");
				return;
			}
			catch (SecurityException)
			{
				Error(nameof(Logger), "Failed to open compressed log for writing: missing permissions.");
				return;
			}

			using (latest)
			using (compressed)
			using (GZipStream zip = new GZipStream(compressed, CompressionLevel.Optimal))
			{
				latest.CopyTo(zip);
			}

			try
			{
				File.SetCreationTimeUtc(kLatestFile, DateTime.UtcNow);
			}
			catch (UnauthorizedAccessException)
			{
				Error(nameof(Logger), "Failed to set latest log's date.");
				return;
			}

			sw.Stop();
			UmfDebug(nameof(Logger), $"Compression and write took {sw.ElapsedMilliseconds}ms");
		}

		/// <summary>
		/// Fired when a message is logged.
		/// </summary>
		public static event Action<LogMessage> Invoked;

		/// <summary>
		/// Logs an entry with INFO severity.
		/// </summary>
		/// <param name="source">The origin of the entry; who's logging this?</param>
		/// <param name="message">The data of the entry; what's being logged?</param>
		public static void Info(string source, string message) =>
			Log(Severity.Info, source, message);

		/// <summary>
		/// Logs an entry with INFO severity but does not run the event.
		/// </summary>
		/// <param name="source">The origin of the entry; who's logging this?</param>
		/// <param name="message">The data of the entry; what's being logged?</param>
		public static void InfoSilent(string source, string message) =>
			LogSilent(Severity.Info, source, message);

		/// <summary>
		/// Logs an entry with WARNING severity.
		/// </summary>
		/// <param name="source">The origin of the entry; who's logging this?</param>
		/// <param name="message">The data of the entry; what's being logged?</param>
		public static void Warning(string source, string message) =>
			Log(Severity.Warning, source, message);

		/// <summary>
		/// Logs an entry with WARNING severity but does not run the event.
		/// </summary>
		/// <param name="source">The origin of the entry; who's logging this?</param>
		/// <param name="message">The data of the entry; what's being logged?</param>
		public static void WarningSilent(string source, string message) =>
			LogSilent(Severity.Warning, source, message);

		/// <summary>
		/// Logs an entry with ERROR severity.
		/// </summary>
		/// <param name="source">The origin of the entry; who's logging this?</param>
		/// <param name="message">The data of the entry; what's being logged?</param>
		public static void Error(string source, string message) =>
			Log(Severity.Error, source, message);

		/// <summary>
		/// Logs an entry with ERROR severity but does not run the event.
		/// </summary>
		/// <param name="source">The origin of the entry; who's logging this?</param>
		/// <param name="message">The data of the entry; what's being logged?</param>
		public static void ErrorSilent(string source, string message) =>
			LogSilent(Severity.Error, source, message);

		/// <summary>
		/// Logs an entry with DEBUG severity and checks the debug mode of UMF.
		/// </summary>
		/// <param name="source">The origin of the entry; who's logging this?</param>
		/// <param name="message">The data of the entry; what's being logged?</param>
		public static void UmfDebug(string source, string message)
		{
			LogMessage data = LogSilent(Severity.Debug, source, message);

			if (UmfCore.Instance.Settings.debug)
			{
				LogEvent(data);
			}
		}

		/// <summary>
		/// Logs an entry with DEBUG severity.
		/// </summary>
		/// <param name="source">The origin of the entry; who's logging this?</param>
		/// <param name="message">The data of the entry; what's being logged?</param>
		public static void Debug(string source, string message) =>
			Log(Severity.Debug, source, message);

		private static string FormatMessage(DateTime time, LogMessage message) =>
			$"[{time:yyyy-MM-dd hh:mm:ss}] {message}";

		private static LogMessage LogSilent(Severity severity, string source, string message)
		{
			LogMessage data = new LogMessage
			{
				severity = severity,
				source = source,
				message = message
			};

			_file?.WriteLine(FormatMessage(DateTime.Now, data));

			return data;
		}

		private static void LogEvent(LogMessage data)
		{
			try
			{
				Invoked?.Invoke(data);
			}
			catch (Exception e)
			{
				LogSilent(Severity.Error, nameof(Logger), $"Error while running the log event:\n{e}");
			}
		}

		private static void Log(Severity severity, string source, string message) =>
			LogEvent(LogSilent(severity, source, message));
	}
}
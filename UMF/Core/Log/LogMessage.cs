using System;

namespace UMF.Core.Log
{
	/// <summary>
	/// Data about a specific log message.
	/// </summary>
	public struct LogMessage
	{
		private static readonly Array _formats = Enum.GetValues(typeof(Format));

		/// <summary>
		/// The includable information within a formatted log message.
		/// </summary>
		[Flags]
		public enum Format
		{
			/// <summary>
			/// Includes <seealso cref="severity"/>.
			/// </summary>
			Severity = 1 << 0,
			/// <summary>
			/// Includes <seealso cref="source"/>.
			/// </summary>
			Source = 1 << 1,
			/// <summary>
			/// Includes <seealso cref="message"/>.
			/// </summary>
			Message = 1 << 2,
		}

		/// <summary>
		/// Importance of the log message.
		/// </summary>
		public Logger.Severity severity;
		/// <summary>
		/// Where the log message was created from (often <code>nameof&lt;T&gt;</code>).
		/// </summary>
		public string source;
		/// <summary>
		/// The data of the log message.
		/// </summary>
		public string message;

		/// <summary>
		/// Formats the log message with all the data included.
		/// </summary>
		public override string ToString() =>
			$"[{severity}] [{source}] {message}";

		private static string AddSpaced(string current, string addition) =>
			current == null
				? addition
				: current + " " + addition;

		/// <summary>
		/// Formats the log message with only the data specified by the format flags.
		/// </summary>
		/// <param name="format">The collection of <seealso cref="Format"/> flags that specify what data should be included.</param>
		public string ToString(Format format)
		{
			string result = null;
			foreach (Format possibleFormat in _formats)
			{
				if (!format.HasFlag(possibleFormat)) continue;

				switch (possibleFormat)
				{
					case Format.Severity:
						result = AddSpaced(result, $"[{severity}]");
						break;

					case Format.Source:
						result = AddSpaced(result, $"[{source}]");
						break;

					case Format.Message:
						result = AddSpaced(result, message);
						break;
				}
			}

			return result;
		}
	}
}
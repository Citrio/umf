using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using dnlib.DotNet;

namespace UMF.Core
{
	internal class GacBypasser
	{
		public static Assembly CurrentAssembly { get; }  = Assembly.GetExecutingAssembly();


		private readonly Dictionary<string, Assembly> loadedAssemblies;

		public uint Counter { get; private set; }
		public string Postfix { get; set; }

		public IEnumerable<string> AssemblyNames => loadedAssemblies.Keys.Duplicate();

		public IEnumerable<Assembly> Assemblies => loadedAssemblies.Values.Duplicate();

		public GacBypasser()
		{
			loadedAssemblies = new Dictionary<string, Assembly>();

			AppDomain.CurrentDomain.AssemblyResolve += ResolveAssembly;
		}

		~GacBypasser()
		{
			AppDomain.CurrentDomain.AssemblyResolve -= ResolveAssembly;
		}

		private Assembly ResolveAssembly(object sender, ResolveEventArgs args)
		{
			return loadedAssemblies.TryGetValue(CleanName(args.Name), out Assembly assembly)
				? assembly
				: null;
		}

		private string CleanName(string name)
		{
			int gacIndex = name.LastIndexOf(Postfix, StringComparison.Ordinal);
			string clean =
				gacIndex < 0
					? name
					: name.Substring(0, gacIndex);

			return clean;
		}

		private void FixName(ModuleDef module, Stream done)
        {
	        module.Assembly.Name += Postfix + Counter++;
	        module.Write(done);
        }

		public Assembly Load(ModuleDef raw, out string name)
		{
			Assembly assembly;
			name = CleanName(raw.Assembly.Name);

			using (MemoryStream done = new MemoryStream())
			{
				FixName(raw, done);
				assembly = Assembly.Load(done.ToArray());
			}

			loadedAssemblies.Add(name, assembly);

			return assembly;
		}

		public bool TryLoad(ModuleDef raw, out string name, out Assembly loaded)
		{
			if (Contains(CleanName(raw.Assembly.Name)))
			{
				name = null;
				loaded = null;

				return false;
			}

			loaded = Load(raw, out name);

			return true;
		}

		public bool Unload(string name) =>
			loadedAssemblies.Remove(name);

		public bool Contains(string name) =>
			loadedAssemblies.ContainsKey(name);

		public bool Contains(Assembly assembly) =>
			loadedAssemblies.ContainsValue(assembly);

		public Assembly this[string name] =>
			loadedAssemblies[name];
	}
}
using System;

namespace UMF.Core
{
	/// <inheritdoc />
	/// <summary>
	/// Marks a <see cref="T:UMF.Core.UmfModule" /> as an extension. Only one extension may be in a single assembly, and extension assemblies must be placed in the <code>bin</code> folder.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class UmfExtensionAttribute : Attribute { }
}
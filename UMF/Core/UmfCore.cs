using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using dnlib.DotNet;
using UMF.Core.Log;
using UMF.Events;
using UMF.Mods;
using UMF.Settings;

namespace UMF.Core
{
	/// <inheritdoc />
	/// <summary>
	/// Central class of the UMF framework.
	/// </summary>
	public sealed class UmfCore : UmfModule<UmfCoreSettings>
	{
		/// <summary>
		/// Relative path to the directory containing all of UMF.
		/// </summary>
		public const string kUmfDirectory = "UMF/";
		/// <summary>
		/// Relative path to the mods directory.
		/// </summary>
		public const string kModsDirectory = kUmfDirectory + "mods/";

		/// <summary>
		/// Whether or not UMF has fully intialized. If this is false during a <seealso cref="Load"/> or <seealso cref="Unload"/>, it is during the first modload.
		/// </summary>
		public static bool Initialized { get; private set; }

		/// <summary>
		/// The single instance of <see cref="UmfCore"/>.
		/// </summary>
		public static UmfCore Instance { get; private set; }
		/// <summary>
		/// The version of the UMF assembly, found in <code>AssemblyInfo.cs</code>. Accessible here for ease of use.
		/// </summary>
		public static Version AssemblyVersion { get; } = Assembly.GetExecutingAssembly().GetName().Version;


		private static void Init()
		{
			if (Instance != null) throw new InvalidOperationException("Already instantiated.");

			try
			{
				Stopwatch time = Stopwatch.StartNew();

				Instance = new UmfCore();
				Instance.InitModules();

				Instance.InitMods();
				EventModule.Instance.RebuildEvents(); // Do it here instead of Load overload in order to prevent rebuilding 50 times for 50 mods.
				SettingsModule.Instance.RefreshAllSettings(); // Do it here instead of Init because theres nothing loaded yet.

				Initialized = true;

				time.Stop();
				Logger.UmfDebug(nameof(UmfCore), $"Initialization took {time.ElapsedMilliseconds}ms");
			}
			catch (Exception e)
			{
				Logger.Error(nameof(UmfCore), $"Error during initialization:\n{e}");
			}
		}

		private readonly GacBypasser gacBypass;
		private readonly List<Assembly> extensionAssemblies;
		private readonly List<ManagedUmfModule> modules;

		/// <inheritdoc />
		public override string SettingsId { get; } = "Core";

		/// <summary>
		/// All assemblies currently loaded by UMF, including this assembly.
		/// </summary>
		public IEnumerable<Assembly> UmfAssemblies
		{
			get
			{
				yield return GacBypasser.CurrentAssembly;
				foreach (Assembly assembly in extensionAssemblies) yield return assembly;
				foreach (Assembly assembly in gacBypass.Assemblies) yield return assembly;
			}
		}
		/// <summary>
		/// All mod assemblies currently loaded by UMF.
		/// </summary>
		public IEnumerable<Assembly> ModAssemblies => gacBypass.Assemblies.Duplicate();
		/// <summary>
		/// All extension assemblies currently loaded by UMF.
		/// </summary>
		public IEnumerable<Assembly> ExtensionAssemblies => extensionAssemblies.Duplicate();

		private UmfCore()
		{
			gacBypass = new GacBypasser
			{
				Postfix = ", UMF-GACB="
			};
			extensionAssemblies = new List<Assembly>();
			modules = new List<ManagedUmfModule>();
		}

		private void InitModules()
		{
			foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				foreach (Type type in assembly.GetTypes())
				{
					if (type.IsAbstract || typeof(UmfCore) == type || !typeof(UmfModule).IsAssignableFrom(type)) continue;

					bool extension = type.GetCustomAttribute<UmfExtensionAttribute>() != null;

					if (extension && extensionAssemblies.Contains(type.Assembly))
					{
						Logger.Error(nameof(UmfCore), $"Unable to load extension {type}: there is already an extension within the same assembly.");

						continue;
					}

					ManagedUmfModule module;
					try
					{
						module = new ManagedUmfModule(type);
					}
					catch (Exception e)
					{
						Logger.Error(nameof(UmfCore), $"Unable to load module {type}:\n{e}");
						continue;
					}

					module.Init();
					modules.Add(module);

					Logger.UmfDebug(nameof(UmfCore), $"Loaded module {type}");

					if (extension)
					{
						extensionAssemblies.Add(type.Assembly);

						Logger.UmfDebug(nameof(UmfCore), $"Added the assembly of extension {type} to the extension assembly list.");
					}
				}
			}

			SettingsModule.Instance.AddModule(this);
			foreach (ManagedUmfModule module in modules)
			{
				SettingsModule.Instance.AddModule(module.Instance);
			}
		}

		private void InitMods()
		{
			if (!Directory.Exists(kModsDirectory)) return;

			string[] files = Directory.GetFiles(kModsDirectory, "*.dll");
			foreach (string file in Settings.ApplyModLoadSettings(files) ?? files)
			{
				LoadAssembly(Path.GetFullPath(file));
			}
		}

		private void AssemblySanityCheck(string path)
		{
			if (path == null) throw new ArgumentNullException(nameof(path));
			if (string.IsNullOrWhiteSpace(path)) throw new ArgumentException("Empty or whitespace paths are invalid.", nameof(path));
			if (!File.Exists(path)) throw new FileNotFoundException("Could not find the assembly specified.", path);
		}

		/// <summary>
		/// Tries to load an assembly from a path.
		/// </summary>
		/// <param name="path">Path to the assembly.</param>
		/// <exception cref="ArgumentNullException"><paramref name="path"/> is null.</exception>
		/// <exception cref="ArgumentException"><paramref name="path"/> is empty or whitespace.</exception>
		/// <exception cref="FileNotFoundException">Could not find file specified by <paramref name="path"/>.</exception>
		public bool LoadAssembly(string path)
		{
			AssemblySanityCheck(path);

			Assembly assembly;
			string name;

			using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
			using (ModuleDefMD module = ModuleDefMD.Load(file))
			{
				if (!gacBypass.TryLoad(module, out name, out assembly)) return false;
			}

			LoadAssembly(assembly, name, Path.GetFileNameWithoutExtension(path));

			return true;
		}

		private void LoadAssembly(Assembly assembly, string name, string file)
		{
			Logger.UmfDebug(nameof(UmfCore), $"Loading assembly {assembly}");

			Type[] modTypes =
				assembly
					.GetTypes()
					.Where(x => !x.IsAbstract && typeof(UmfMod).IsAssignableFrom(x))
					.ToArray();

			switch (modTypes.Length)
			{
				case 0:
					return;

				case 1:
				{
					if (name != file)
					{
						Logger.Error(nameof(UmfCore), $"Unable to load mod assembly {file}: the file does not have the same file name as the assembly ({name}).");
						return;
					}

					Type modType = modTypes[0];

					ConstructorInfo ctor = modType.GetConstructor(new Type[0]);
					if (ctor == null)
					{
						Logger.Error(nameof(UmfCore), $"Unable to load mod type {modType}: parameterless constructor not found.");
						return;
					}

					UmfMod mod;
					try
					{
						mod = (UmfMod) ctor.Invoke(new object[0]);
					}
					catch (Exception e)
					{
						Logger.Error(nameof(UmfCore), $"Unable to load mod type {modType} due to an exception during initialization:\n{e}");
						return;
					}

					mod.InfoAttribute.Id = name;
					mod.InfoAttribute.AssemblyVersion = assembly.GetName().Version;
					Load(mod);

					break;
				}

				default:
				{
					Logger.Error(nameof(UmfCore), $"Unable to load mod assembly {assembly}: only one non-abstract class can inherit from {typeof(UmfMod)}.");

					return;
				}
			}

			Logger.UmfDebug(nameof(UmfCore), $"Loaded {assembly}.");
		}

		/// <summary>
		/// Unloads an assembly from a path. Note that the code is never "killed", but all events are unhooked and the assembly is no longer in the UMF environment.
		/// </summary>
		/// <param name="path">Path to the assembly.</param>
		/// <exception cref="ArgumentNullException"><paramref name="path"/> is null.</exception>
		/// <exception cref="ArgumentException"><paramref name="path"/> is empty or whitespace.</exception>
		/// <exception cref="FileNotFoundException">Could not find file specified by <paramref name="path"/>.</exception>
		public bool UnloadAssembly(string path)
		{
			AssemblySanityCheck(path);

			string name;

			using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
			using (ModuleDefMD module = ModuleDefMD.Load(file))
			{
				name = module.Assembly.Name;
			}

			if (!gacBypass.Contains(name)) return false;

			UnloadAssembly(gacBypass[name]);
			gacBypass.Unload(name);

			return true;
		}

		private void UnloadAssembly(Assembly assembly)
		{
			Logger.UmfDebug(nameof(UmfCore), $"Unloading assembly: {assembly}.");

			foreach (Type type in assembly.GetTypes())
			{
				if (!typeof(UmfMod).IsAssignableFrom(type) || type.IsAbstract) continue;

				foreach (UmfMod mod in LoadedMods)
				{
					if (mod.GetType() == type)
					{
						Unload(mod);
						break; // Works as long as we don't make multiple instances of the mod.
					}
				}
			}

			Logger.UmfDebug(nameof(UmfCore), $"Unloaded {assembly}.");
		}

		/// <inheritdoc />
		public override bool Load(UmfMod mod)
		{
			if (!base.Load(mod)) return false;

			#if DEBUG
			if (mod.InfoAttribute.Id == null)
			{
				mod.InfoAttribute.Id = "UNIT TEST MOD ONLY";
			}
			#endif

			Logger.UmfDebug(nameof(UmfCore), $"Loading {mod}");

			mod.Status = ModStatus.Loading;
			EventModule.Instance.HandleEvent<LoadStartEvent.OnLoadStart>()?.Invoke(mod);

			foreach (ManagedUmfModule module in modules)
			{
				if (!module.Instance.Load(mod)) return false;
			}

			mod.Status = ModStatus.Loaded;
			EventModule.Instance.HandleEvent<LoadFinishEvent.OnLoadFinish>()?.Invoke(mod);

			Logger.Info(nameof(UmfCore), $"Loaded {mod} successfully");

			return true;
		}

		/// <inheritdoc />
		public override bool Unload(UmfMod mod)
		{
			if (!base.Unload(mod)) return false;

			Logger.UmfDebug(nameof(UmfCore), $"Unloading {mod}");

			mod.Status = ModStatus.Unloading;
			EventModule.Instance.HandleEvent<UnloadStartEvent.OnUnloadStart>()?.Invoke(mod);

			foreach (ManagedUmfModule module in modules)
			{
				if (!module.Instance.Unload(mod)) return false;
			}

			mod.Status = ModStatus.Unloaded;
			EventModule.Instance.HandleEvent<UnloadFinishEvent.OnUnloadFinish>()?.Invoke(mod);

			Logger.Info(nameof(UmfCore), $"Unloaded {mod} successfully");

			return true;
		}
	}
}
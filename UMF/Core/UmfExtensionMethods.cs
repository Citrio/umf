using System.Collections.Generic;

namespace UMF.Core
{
	/// <summary>
	/// Collection of extension methods used by UMF.
	/// </summary>
	public static class UmfExtensionMethods
	{
		/// <summary>
		/// Duplicates an <see cref="IEnumerable{T}"/> to ensure the original enumerable is not altered (likely a list or array).
		/// </summary>
		/// <param name="enumerable">Enumerable to duplicate.</param>
		/// <typeparam name="T">Type of the enumerable to duplicate.</typeparam>
		public static IEnumerable<T> Duplicate<T>(this IEnumerable<T> enumerable)
		{
			if (enumerable == null) yield break;

			foreach (T item in enumerable)
			{
				yield return item;
			}
		}
	}
}
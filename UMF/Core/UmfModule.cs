using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UMF.Mods;
using UMF.Settings;

namespace UMF.Core
{
	/// <inheritdoc />
	/// <summary>
	/// A module of the UMF framework.
	/// </summary>
	public abstract class UmfModule : ISettingsAcceptor
	{
		private readonly List<UmfMod> loadedMods;

		/// <summary>
		/// The expected method signature of the static initializer.
		/// </summary>
		public delegate void OnInit();

		/// <summary>
		/// The mods that the module is currently responsible for.
		/// </summary>
		public IEnumerable<UmfMod> LoadedMods => loadedMods.Duplicate();

		/// <inheritdoc />
		public virtual JObject DefaultJsonSettings { get; } = new JObject();

		private JObject jsonSettings;
		/// <inheritdoc />
		public virtual JObject JsonSettings
		{
			get => jsonSettings ?? DefaultJsonSettings;
			set => jsonSettings = value;
		}

		/// <summary>
		/// The name of the settings file after the UMF namespace in the format of <code>UMF.</code><seealso cref="SettingsId"/><code>.json</code>.
		/// </summary>
		public abstract string SettingsId { get; }

		/// <summary>
		/// Creates an instance of the module.
		/// </summary>
		protected UmfModule()
		{
			loadedMods = new List<UmfMod>();
		}

		/// <summary>
		/// Loads a mod into the module and holds the module responsible for maintaining it.
		/// </summary>
		/// <param name="mod">The mod to load into the module.</param>
		/// <exception cref="ArgumentNullException"><paramref name="mod"/> is null.</exception>
		/// <returns>Whether or not the mod successfully loaded (if not, it was already loaded).</returns>
		public virtual bool Load(UmfMod mod)
		{
			if (mod == null) throw new ArgumentNullException(nameof(mod));

			if (!loadedMods.Contains(mod))
			{
				loadedMods.Add(mod);
				return true;
			}

			return false;
		}

		/// <summary>
		/// Unloads a mod from the module and releases the responsibility of the module for maintaining it.
		/// </summary>
		/// <param name="mod">The mod to unload from the module.</param>
		/// <exception cref="ArgumentNullException"><paramref name="mod"/> is null.</exception>
		/// <returns>Whether or not the mod successfully unloaded (if not, it wasn't loaded in the first place).</returns>
		public virtual bool Unload(UmfMod mod)
		{
			if (mod == null) throw new ArgumentNullException(nameof(mod));

			return loadedMods.Remove(mod);
		}
	}

	/// <inheritdoc />
	/// <summary>
	/// A module of the UMF framework with typed settings.
	/// </summary>
	/// <typeparam name="TSettings">Type of the settings.</typeparam>
	public abstract class UmfModule<TSettings> : UmfModule where TSettings : class, new()
	{
		/// <summary>
		/// The fallback of <seealso cref="Settings"/> if it is null.
		/// </summary>
		public static TSettings DefaultSettings { get; } = new TSettings();

		/// <inheritdoc />
		public override JObject DefaultJsonSettings { get; } = JObject.FromObject(DefaultSettings);

		/// <inheritdoc />
		public override JObject JsonSettings
		{
			get => base.JsonSettings;
			set
			{
				settings = value?.ToObject<TSettings>();
				base.JsonSettings = value;
			}
		}

		private TSettings settings;

		/// <summary>
		/// Typed representation of the settings file. During the <code>get</code>, it falls back to <seealso cref="DefaultSettings"/> if the settings are null.
		/// </summary>
		public TSettings Settings
		{
			get => settings ?? DefaultSettings;
			protected set =>
				JsonSettings =
					value == null
						? null
						: JObject.FromObject(value);
		}
	}
}
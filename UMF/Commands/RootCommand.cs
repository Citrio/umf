using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using UMF.Commands.Arguments;
using UMF.Commands.Exceptions;
using UMF.Core;
using UMF.Mods;
using UMF.Settings;

namespace UMF.Commands
{
	internal class RootCommand : UmfCommand
	{
		private readonly Dictionary<string, UmfCommand> commands;

		public override string Description { get; } = "Lists all UMF commands.";

		public ValueArgument Mode { get; }
		public ValueArgument Target { get; }

		public RootCommand(Dictionary<string, UmfCommand> commands) : base("umf")
		{
			this.commands = commands;

			MandatoryArguments.Add(Mode = new ValueArgument("mode", "Action to perform"));
			OptionalArguments.Add(Target = new ValueArgument("target", "Value or target to apply to the mode"));
		}

		protected override string Execute()
		{
			switch (Mode.Value)
			{
				case "debug":
				{
					if (Target.HasValue)
					{
						if (bool.TryParse(Target.Value, out bool enabled))
						{
							UmfCore.Instance.Settings.debug = enabled;
						}
						else
						{
							throw new CommandParameterException(Target, "Failed to parse.");
						}
					}

					return $"Debug mode: {UmfCore.Instance.Settings.debug}";
				}

				case "help":
				{
					StringBuilder builder = new StringBuilder();

					foreach (UmfCommand command in commands.Values)
					{
						builder.Append(command.Alias);
						builder.Append(" - ");
						builder.Append(command.Description);

						builder.AppendLine();
					}

					return builder.ToString();
				}

				case "mods":
				{
					StringBuilder builder = new StringBuilder();

					foreach (UmfMod mod in UmfCore.Instance.LoadedMods)
					{
						builder.AppendLine(mod.ToString());
					}

					return builder.ToString();
				}

				case "refresh":
				{
					if (!Target.HasValue)
					{
						SettingsModule.Instance.RefreshAllSettings();

						return "All settings refreshed.";
					}

					SettingsModule.Instance.RefreshSettings(Target.Value);

					return $"Refreshed {Target.Value}.";
				}

				case "save":
				{
					if (!Target.HasValue)
					{
						SettingsModule.Instance.SaveAllSettings();

						return "All settings saved.";
					}

					SettingsModule.Instance.SaveSettings(Target.Value);

					return $"Saved {Target.Value}.";
				}

				case "load":
					return StandardizedModIO("Load", UmfCore.Instance.LoadAssembly);

				case "unload":
					return StandardizedModIO("Unload", UmfCore.Instance.UnloadAssembly);

				default:
					throw new CommandParameterException(Mode, "Mode not found.");
			}
		}

		private string StandardizedModIO(string mode, Func<string, bool> ioOperation)
		{
			if (!Target.HasValue) throw new CommandParameterException(Target, $"{mode}ing requires a target.");

			string path = UmfCore.kModsDirectory + Target + ".dll";
			if (!File.Exists(path)) throw new CommandParameterException(Target, "File not found.");

			Stopwatch sw = Stopwatch.StartNew();
			bool result = ioOperation(path);
			sw.Stop();

			if (!result) throw new CommandException($"Failed to {mode.ToLower()}.");

			return $"{mode}ed successfully ({sw.ElapsedMilliseconds}ms).";
		}
	}
}
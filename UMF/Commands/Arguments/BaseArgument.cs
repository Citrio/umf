using System;

namespace UMF.Commands.Arguments
{
	/// <inheritdoc />
	/// <summary>
	/// An argument to a command.
	/// </summary>
	public abstract class BaseArgument : CommandParameter
	{
		/// <summary>
		/// The name or identifier of the argument, for use in help pages.
		/// </summary>
		public string Name { get; }
		/// <summary>
		/// The description of what this argument represents, for use in help pages.
		/// </summary>
		public string Description { get; }

		/// <summary>
		/// Whether or not the argument has been set this command call.
		/// </summary>
		public virtual bool HasValue { get; protected set; }

		/// <summary>
		/// Creates a new instance of <see cref="BaseArgument"/>.
		/// </summary>
		/// <param name="name"><seealso cref="Name"/></param>
		/// <param name="description"><seealso cref="Description"/></param>
		protected BaseArgument(string name, string description)
		{
			if (name == null) throw new ArgumentNullException(nameof(name));
			if (string.IsNullOrWhiteSpace(name)) throw new ArgumentException("Cannot be empty or whitespace.", nameof(name));
			if (description != null && string.IsNullOrWhiteSpace(description)) throw new ArgumentException("Cannot be empty or whitespace.", nameof(name));

			Name = name;
			Description = description;
		}

		/// <inheritdoc />
		public override string ToString() =>
			Name;
	}
}
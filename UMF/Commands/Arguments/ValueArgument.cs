using System;
using System.Collections.Generic;
using UMF.Commands.Exceptions;
using UMF.Commands.Results;

namespace UMF.Commands.Arguments
{
	/// <inheritdoc />
	/// <summary>
	/// An argument to a command that represents a typed object of a string input.
	/// </summary>
	/// <typeparam name="T">Type of the object that the argument represents.</typeparam>
	public class ValueArgument<T> : BaseArgument
	{
		private readonly Func<string, ParseResult<T>> parser;

		private T value;
		/// <summary>
		/// The value of the argument within this command call.
		/// </summary>
		/// <exception cref="InvalidOperationException">Argument has no value. Likely due to accessing the command out of a command call or when accessing an optional argument that was not specified.</exception>
		public T Value
		{
			get
			{
				if (!HasValue) throw new InvalidOperationException("Argument is not set.");

				return value;
			}
			set => this.value = value;
		}

		/// <inheritdoc />
		/// <summary>
		/// Creates an instance of <see cref="ValueArgument{T}"/>.
		/// </summary>
		/// <param name="parser">The function to convert the raw string data into this argument's object type.</param>
		/// <param name="name"><seealso cref="BaseArgument.Name" /></param>
		/// <param name="description"><seealso cref="BaseArgument.Description" /></param>
		public ValueArgument(Func<string, ParseResult<T>> parser, string name, string description) : base(name, description)
		{
			if (parser == null) throw new ArgumentNullException(nameof(parser));

			this.parser = parser;
		}

		/// <inheritdoc />
		/// <exception cref="CommandParameterException">The input could not be parsed successfully.</exception>
		protected override void UpdateSafe(IReadOnlyList<string> args, ref int index)
		{
			ParseResult<T> result = parser(args[index]);
			if (!result.success) throw new CommandParameterException(this, "Failed to parse.");

			Value = result.value;
			HasValue = true;
		}

		/// <inheritdoc />
		public override void Clear()
		{
			Value = default;
			HasValue = false;
		}
	}

	/// <inheritdoc />
	/// <summary>
	/// An argument that represents the raw string data of a string input.
	/// </summary>
	public class ValueArgument : ValueArgument<string>
	{
		/// <inheritdoc />
		public ValueArgument(string name, string description) : base(ParseResult<string>.FromSuccess, name, description) { }
	}
}
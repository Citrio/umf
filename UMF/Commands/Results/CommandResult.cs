namespace UMF.Commands.Results
{
	/// <summary>
	/// The resulting info from a <see cref="CommandModule"/> command call.
	/// </summary>
	public struct CommandResult
	{
		/// <summary>
		/// Whether or not the command was found.
		/// </summary>
		public bool found;
		/// <summary>
		/// Whether or not the command executed without error.
		/// </summary>
		public bool success;
		/// <summary>
		/// The reply of the command.
		/// </summary>
		public string message;

		/// <summary>
		/// Explicitly set <seealso cref="found"/> to <code>false</code>.
		/// </summary>
		public static CommandResult NotFound { get; } = new CommandResult
		{
			found = false
		};

		/// <summary>
		/// Explicitly sets <seealso cref="found"/> to <code>true</code> and <seealso cref="success"/> to <code>false</code>.
		/// </summary>
		/// <param name="message">The value of <seealso cref="message"/>; the error message.</param>
		public static CommandResult FromError(string message) =>
			new CommandResult
			{
				found = true,
				success = false,
				message = message
			};

		/// <summary>
		/// Explicitly sets <seealso cref="found"/> to <code>true</code> and <seealso cref="success"/> to <code>true</code>.
		/// </summary>
		/// <param name="message">The value of <seealso cref="message"/>; the reply.</param>
		public static CommandResult FromSuccess(string message) =>
			new CommandResult
			{
				found = true,
				success = true,
				message = message
			};
	}
}
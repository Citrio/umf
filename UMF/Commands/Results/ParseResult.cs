namespace UMF.Commands.Results
{
	/// <summary>
	/// The resulting info from a typed object parser.
	/// </summary>
	/// <typeparam name="T">Type of the object that was parsed.</typeparam>
	public struct ParseResult<T>
	{
		/// <summary>
		/// Whether or not it was able to parse the input.
		/// </summary>
		public bool success;
		/// <summary>
		/// The result of the parse (used if successful).
		/// </summary>
		public T value;

		/// <summary>
		/// Explicitly set <seealso cref="success"/> to <code>false</code>.
		/// </summary>
		public static ParseResult<T> Error { get; } = new ParseResult<T>
		{
			success = false
		};

		/// <summary>
		/// Explicitly set <seealso cref="success"/> to <code>true</code>.
		/// </summary>
		/// <param name="value">The value of <seealso cref="value"/>; the resulting data from the parse.</param>
		public static ParseResult<T> FromSuccess(T value) =>
			new ParseResult<T>
			{
				success = true,
				value = value
			};
	}
}
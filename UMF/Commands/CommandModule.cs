using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UMF.Commands.Exceptions;
using UMF.Commands.Results;
using UMF.Core;
using UMF.Core.Log;
using UMF.Mods;

namespace UMF.Commands
{
	/// <inheritdoc />
	/// <summary>
	/// The module for handling all UMF commands from the console.
	/// </summary>
	public sealed class CommandModule : UmfModule
	{
		private static readonly Regex _argsParser = new Regex("[^\\s\"\']+|\"([^\"]*)\"|\'([^\']*)\'");

		/// <summary>
		/// The single instance of <see cref="CommandModule"/>.
		/// </summary>
		public static CommandModule Instance { get; private set; }

		/// <inheritdoc />
		public override string SettingsId { get; } = "Commands";

		private static void Init()
		{
			if (Instance != null) throw new InvalidOperationException("Already instantiated.");

			Instance = new CommandModule();
		}

		private readonly Dictionary<UmfMod, UmfCommand[]> modCommands;
		private readonly Dictionary<string, UmfCommand> commands;

		private CommandModule()
		{
			modCommands = new Dictionary<UmfMod, UmfCommand[]>();
			commands = new Dictionary<string, UmfCommand>();

			commands.Add("umf", new RootCommand(commands));
		}

		/// <summary>
		/// Handles the raw input from a command line and processes the data through a command, if it exists.
		/// </summary>
		/// <param name="command">The raw data from the command line.</param>
		/// <exception cref="ArgumentNullException"><paramref name="command"/> is null.</exception>
		public CommandResult Handle(string command)
		{
			if (command == null) throw new ArgumentNullException(nameof(command));
			if (string.IsNullOrEmpty(command)) return CommandResult.NotFound;

			Logger.UmfDebug(nameof(CommandModule), $"Handling command: {command}");

			MatchCollection collection = _argsParser.Matches(command);
			string[] args = new string[collection.Count];

			for (int i = 0; i < collection.Count; i++)
			{
				// If it's wrapped in quotes,
				if (collection[i].Value[0] == '\"' && collection[i].Value[collection[i].Value.Length - 1] == '\"')
				{
					args[i] = collection[i].Value.Substring(1, collection[i].Value.Length - 2);
				}
				else
				{
					args[i] = collection[i].Value;
				}
			}

			UmfCommand handler = this[args[0].ToLower()];
			if (handler != null)
			{
				string result;

				try
				{
					result = handler.Execute(args);
				}
				catch (CommandException e)
				{
					return CommandResult.FromError(e.Message);
				}
				catch (Exception e)
				{
					Logger.ErrorSilent(nameof(CommandModule), $"{handler.Name} with input \"{command}\":\n{e}");
					return CommandResult.FromError($"Unhandled exception (logged):\n{e}");
				}

				return CommandResult.FromSuccess(result);
			}

			return CommandResult.NotFound;
		}

		private bool AddCommand(UmfMod mod, UmfCommand command)
		{
			if (command.Owner != null) throw new ArgumentException("Command has already been added.", nameof(command));

			string alias = mod.ModSettings.ApplyCommand(command.Name).ToLower();

			if (commands.ContainsKey(alias))
			{
				Logger.Error(nameof(CommandModule), $"{mod} tried to register a command, but the name was already taken. Set an alias in the mod settings.");

				return false;
			}

			command.alias = alias;
			command.Owner = mod;
			commands.Add(alias, command);

			return true;
		}

		private bool RemoveCommand(UmfCommand command)
		{
			if (commands.Remove(command.alias))
			{
				command.alias = null;
				command.Owner = null;

				return true;
			}

			return false;
		}

		/// <inheritdoc />
		public override bool Load(UmfMod mod)
		{
			if (!base.Load(mod)) return false;

			UmfCommand[] commandsFromMod = mod.Commands.ToArray();

			foreach (UmfCommand command in commandsFromMod)
			{
				AddCommand(mod, command);
			}

			modCommands.Add(mod, commandsFromMod);

			return true;
		}

		/// <inheritdoc />
		public override bool Unload(UmfMod mod)
		{
			if (!base.Unload(mod)) return false;

			foreach (UmfCommand command in modCommands[mod])
			{
				RemoveCommand(command);
			}

			modCommands.Remove(mod);

			return true;
		}

		/// <summary>
		/// Finds a command by name (null if non-existant).
		/// </summary>
		/// <param name="name">Name of the command (case insensitive).</param>
		public UmfCommand this[string name] =>
			commands.TryGetValue(name.ToLower(), out UmfCommand command)
				? command
				: null;
	}
}
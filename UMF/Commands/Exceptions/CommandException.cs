using System;

namespace UMF.Commands.Exceptions
{
	/// <inheritdoc />
	/// <summary>
	/// The exception that is thrown when a command cannot process a command successfully.
	/// </summary>
	public class CommandException : Exception
	{
		/// <inheritdoc />
		public CommandException(string message) : base(message) { }
	}
}
namespace UMF.Commands.Exceptions
{
	/// <inheritdoc />
	/// <summary>
	/// The exception that is thrown when a command cannot process a command successfully due to a bad parameter.
	/// </summary>
	public class CommandParameterException : CommandException
	{
		/// <inheritdoc />
		public CommandParameterException(CommandParameter argument, string message) : base($"{argument}: {message}") { }
	}
}
using System;
using System.Collections.Generic;

namespace UMF.Commands
{
	/// <summary>
	/// An input for a command.
	/// </summary>
	public abstract class CommandParameter
	{
		/// <summary>
		/// Updates the data of this parameter from the specified string list.
		/// </summary>
		/// <param name="args">All available parameters (some parameters may require context of others).</param>
		/// <param name="index">The index of this parameter.</param>
		/// <exception cref="ArgumentNullException"><paramref name="args"/> is null.</exception>
		/// <exception cref="ArgumentException"><paramref name="index"/> is out of <paramref name="args"/>'s range.</exception>
		public void Update(IReadOnlyList<string> args, ref int index)
		{
			UpdateSanityCheck(args, index);
			UpdateSafe(args, ref index);
		}

		/// <summary>
		/// Clears all data of this parameter.
		/// </summary>
		public abstract void Clear();


		/// <summary>
		/// Checks the parameters of <seealso cref="UpdateSafe"/> and throws exceptions if they are invalid.
		/// </summary>
		protected void UpdateSanityCheck(IReadOnlyList<string> args, int index)
		{
			if (args == null) throw new ArgumentNullException(nameof(args));
			if (index < 0 || args.Count <= index) throw new ArgumentException("Out of arguments' range", nameof(index));
		}

		/// <summary>
		/// Updates the data of this parameter from the specified string list. Does not perform the null and out-of-bounds check
		/// </summary>
		/// <param name="args">All available parameters (some parameters may require context of others).</param>
		/// <param name="index">The index of this parameter.</param>
		protected abstract void UpdateSafe(IReadOnlyList<string> args, ref int index);
	}
}
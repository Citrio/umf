using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UMF.Commands.Arguments;
using UMF.Commands.Exceptions;
using UMF.Commands.Options;
using UMF.Core;
using UMF.Core.Log;
using UMF.Mods;

namespace UMF.Commands
{
	/// <summary>
	/// A command, part of the <see cref="CommandModule"/>.
	/// </summary>
	public abstract class UmfCommand
	{
		private UmfMod owner;
		/// <summary>
		/// The <see cref="UmfMod"/> that owns the command.
		/// </summary>
		public UmfMod Owner
		{
			get => owner;
			internal set
			{
				try
				{
					OnOwnerSet(value);
				}
				catch (Exception e)
				{
					Logger.Error(nameof(UmfCommand), $"Failed to set owner of {this}:\n{e}");
				}

				owner = value;
			}
		}

		/// <summary>
		/// The option to show the help page (--help). Bypasses all argument checks. Updated by populator.
		/// </summary>
		public BaseOption HelpOption { get; }

		/// <summary>
		/// Options updated by the populator.
		/// </summary>
		protected List<BaseOption> Options { get; }
		/// <summary>
		/// Arguments updated by the populator. An error will be returned if all of these are not updated.
		/// </summary>
		protected List<BaseArgument> MandatoryArguments { get; }
		/// <summary>
		/// Arguments updated by the populator.
		/// </summary>
		protected List<BaseArgument> OptionalArguments { get; }

		/// <summary>
		/// All options updated by the populator.
		/// </summary>
		public IEnumerable<BaseOption> AllOptions
		{
			get
			{
				yield return HelpOption;
				foreach (BaseOption option in Options) yield return option;
			}
		}
		/// <summary>
		/// All arguments updated by the populator.
		/// </summary>
		public IEnumerable<BaseArgument> AllArguments => MandatoryArguments.Duplicate().Concat(OptionalArguments.Duplicate());

		/// <summary>
		/// Case-insensitive name of the command.
		/// </summary>
		public string Name { get; }

		internal string alias;
		/// <summary>
		/// User-set alias for the command. If none exists, returns <seealso cref="Name"/>.
		/// </summary>
		public string Alias => alias ?? Name;

		/// <summary>
		/// Creates usage example from <seealso cref="Options"/>, <seealso cref="MandatoryArguments"/>, and <seealso cref="OptionalArguments"/>.
		/// </summary>
		public string Usage
		{
			get
			{
				StringBuilder builder = new StringBuilder();

				builder.Append(Name);

				if (Options.Count > 0)
				{
					builder.Append(" [...]");
				}

				foreach (BaseArgument argument in MandatoryArguments)
				{
					builder.Append(" (");
					builder.Append(argument);
					builder.Append(')');
				}

				foreach (BaseArgument argument in OptionalArguments)
				{
					builder.Append(" [");
					builder.Append(argument);
					builder.Append(']');
				}

				return builder.ToString();
			}
		}
		/// <summary>
		/// An explanation of what this command does.
		/// </summary>
		public abstract string Description { get; }

		/// <summary>
		/// Creates an instance of <see cref="UmfCommand"/>.
		/// </summary>
		/// <param name="name"><seealso cref="Name"/></param>
		protected UmfCommand(string name)
		{
			if (name == null) throw new ArgumentNullException(nameof(name));
			if (string.IsNullOrWhiteSpace(name)) throw new ArgumentException("", nameof(name));

			Name = name;

			HelpOption = new ToggleOption(null, "help", "Lists information about this command.");

			Options = new List<BaseOption>();
			MandatoryArguments = new List<BaseArgument>();
			OptionalArguments = new List<BaseArgument>();
		}

		/// <summary>
		/// Fired when the owner of the command is changed.
		/// </summary>
		/// <param name="mod">New owner of the command.</param>
		protected virtual void OnOwnerSet(UmfMod mod) { }

		/// <summary>
		/// Populates all options and arguments of this command from the array.
		/// </summary>
		/// <param name="args">Raw command line arguments.</param>
		/// <exception cref="ArgumentException"><paramref name="args"/> is null.</exception>
		/// <exception cref="CommandException">Too many arguments are provided.</exception>
		protected void Populate(IReadOnlyList<string> args)
		{
			if (args == null) throw new ArgumentNullException(nameof(args));

			BaseArgument[] arguments = AllArguments.ToArray();
			BaseOption[] options = AllOptions.ToArray();

			foreach (BaseOption baseSwitch in options) baseSwitch.Clear();
			foreach (BaseArgument baseArgument in arguments) baseArgument.Clear();

			int arg = 0;
			for (int i = 1; i < args.Count; i++)
			{
				bool switched = false;

				int newI = i;
				foreach (BaseOption baseSwitch in Options)
				{
					int offset = i;
					switched = baseSwitch.TryUpdate(args, ref offset) || switched;

					if (offset > newI)
					{
						newI = offset;
					}
				}

				if (!switched)
				{
					if (arguments.Length <= arg) throw new CommandException("Too many arguments.");

					arguments[arg++].Update(args, ref i);
				}
				else
				{
					i = newI;
				}
			}
		}

		/// <summary>
		/// Generates a help page with indentation and space alignment.
		/// </summary>
		public string GenerateHelp()
		{
			StringBuilder builder = new StringBuilder();

			builder.Append("From: ");
			builder.AppendLine(Owner?.ToString() ?? "UMF");
			builder.AppendLine();

			builder.Append("Usage: ");
			builder.AppendLine(Usage);
			builder.AppendLine(Description);

			builder.AppendLine();
			builder.AppendLine("Arguments:");
			builder.AppendLine();

			int longestName = 0;

			foreach (BaseArgument argument in AllArguments)
			{
				if (longestName < argument.Name.Length)
				{
					longestName = argument.Name.Length;
				}
			}

			foreach (BaseOption option in AllOptions)
			{
				if (option.LongName == null) continue;

				int length = option.LongName.Length + 2;
				if (longestName < length)
				{
					longestName = length;
				}
			}

			foreach (BaseArgument argument in AllArguments)
			{
				builder.Append("     ");

				builder.Append(argument.Name);
				builder.Append(new string(' ', longestName - argument.Name.Length));

				if (argument.Description != null)
				{
					builder.Append("  ");
					builder.Append(argument.Description);
				}

				builder.AppendLine();
			}

			builder.AppendLine();
			builder.AppendLine("Options:");
			builder.AppendLine();

			int longestOptionName = longestName - 2;
			string longOptionNameFiller = new string(' ', longestOptionName);

			foreach (BaseOption option in Options)
			{
				builder.Append("  ");

				if (option.ShortName != null)
				{
					builder.Append('-');
					builder.Append(option.ShortName.Value);
					builder.Append(' ');
				}
				else
				{
					builder.Append("   ");
				}

				if (option.LongName != null)
				{
					builder.Append("--");
					builder.Append(option.LongName);
					builder.Append(
						new string(
							' ',
							longestOptionName - option.LongName.Length
						)
					);
				}
				else
				{
					builder.Append(longOptionNameFiller);
				}

				builder.Append("  ");

				if (option.Description != null)
				{
					builder.Append(option.Description);
				}

				builder.AppendLine();
			}

			return builder.ToString();
		}

		/// <summary>
		/// Completely processes raw command line arguments and executes the command with them populated.
		/// </summary>
		/// <param name="args">Raw command line arguments.</param>
		/// <exception cref="CommandException">Too few arguments OR a user thrown exception, representing an error during command execution.</exception>
		public string Execute(IReadOnlyList<string> args)
		{
			Populate(args);

			if (HelpOption.HasValue)
			{
				return GenerateHelp();
			}

			if (MandatoryArguments.Count > 0 && !MandatoryArguments[MandatoryArguments.Count - 1].HasValue) throw new CommandException("Too few arguments.");

			return Execute();
		}

		/// <summary>
		/// Runs the command within the current context.
		/// </summary>
		protected abstract string Execute();

		/// <inheritdoc />
		public override string ToString() => 
			Name;
	}

	/// <inheritdoc />
	/// <summary>
	/// A command, part of the <see cref="T:UMF.Commands.CommandModule" />, that has a typed owner.
	/// </summary>
	/// <typeparam name="TMod">Type of the owning mod.</typeparam>
	public abstract class UmfCommand<TMod> : UmfCommand where TMod : UmfMod
	{
		/// <summary>
		/// The <see cref="UmfMod"/> that owns the command.
		/// </summary>
		public new TMod Owner { get; private set; }

		/// <inheritdoc />
		protected UmfCommand(string name) : base(name) { }

		/// <inheritdoc />
		protected override void OnOwnerSet(UmfMod mod) =>
			Owner = (TMod) mod;
	}
}
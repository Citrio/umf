using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMF.Commands.Options
{
	/// <inheritdoc />
	/// <summary>
	/// An option to a command.
	/// </summary>
	public abstract class BaseOption : CommandParameter
	{
		/// <summary>
		/// The single-character name of an option, used with one dash.
		/// </summary>
		public char? ShortName { get; }
		/// <summary>
		/// The multi-character name of an option, used with two dashes.
		/// </summary>
		public string LongName { get; }
		/// <summary>
		/// The description of what the option does, used in help pages.
		/// </summary>
		public string Description { get; }

		/// <summary>
		/// Whether or not the option is set this command call.
		/// </summary>
		public bool HasValue { get; protected set; }

		/// <summary>
		/// Creates an instance of <see cref="BaseOption"/>.
		/// </summary>
		/// <param name="shortName"><seealso cref="ShortName"/></param>
		/// <param name="longName"><seealso cref="LongName"/></param>
		/// <param name="description"><seealso cref="Description"/></param>
		protected BaseOption(char? shortName, string longName, string description)
		{
			ShortName = shortName;
			LongName = longName;
			Description = description;
		}

		/// <summary>
		/// Attempts to Updates the data of this option from the specified string list.
		/// </summary>
		/// <param name="args">All available arguments (some options may require context of others).</param>
		/// <param name="index">The index of this option.</param>
		/// <exception cref="ArgumentNullException"><paramref name="args"/> is null.</exception>
		/// <exception cref="ArgumentException"><paramref name="index"/> is out of <paramref name="args"/>'s range.</exception>
		/// <returns>Whether or not the option was updated.</returns>
		public bool TryUpdate(IReadOnlyList<string> args, ref int index)
		{
			UpdateSanityCheck(args, index);

			string raw = args[index];

			if (!string.IsNullOrWhiteSpace(raw) && raw.Length > 1 && raw[0] == '-')
			{
				if (raw[1] == '-') // --
				{
					if (LongName != null && raw.Substring(2) == LongName) // Matches long name
					{
						UpdateSafe(args, ref index);

						return true;
					}
				}
				else if (ShortName != null && raw.Substring(1).Contains(ShortName.Value)) // - and contains short name
				{
					UpdateSafe(args, ref index);

					return true;
				}
			}

			return false;
		}

		/// <inheritdoc />
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();

			if (ShortName != null)
			{
				builder.Append('-');
				builder.Append(ShortName.Value);

				if (LongName != null)
				{
					builder.Append(' ');
				}
			}

			if (LongName != null)
			{
				builder.Append("--");
				builder.Append(LongName);
			}

			return builder.ToString();
		}
	}
}
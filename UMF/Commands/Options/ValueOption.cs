using System;
using System.Collections.Generic;
using UMF.Commands.Exceptions;
using UMF.Commands.Results;

namespace UMF.Commands.Options
{
	/// <inheritdoc />
	/// <summary>
	/// An option to a command that represents a typed object of a string input.
	/// </summary>
	/// <typeparam name="T">Type of the object that the option represents.</typeparam>
	public class ValueOption<T> : BaseOption
	{
		private readonly Func<string, ParseResult<T>> parser;

		private T value;
		/// <summary>
		/// The value of the option within this command call.
		/// </summary>
		/// <exception cref="InvalidOperationException">Option has no value. Likely due to accessing the option out of this command call or when accessing an option that was not specified.</exception>
		public T Value
		{
			get
			{
				if (!HasValue) throw new InvalidOperationException("Switch is not set.");

				return value;
			}
			set => this.value = value;
		}

		/// <inheritdoc />
		public ValueOption(Func<string, ParseResult<T>> parser, char? shortName, string longName, string description) : base(shortName, longName, description)
		{
			this.parser = parser;
		}

		/// <inheritdoc />
		protected override void UpdateSafe(IReadOnlyList<string> args, ref int index)
		{
			if (index + 1 >= args.Count) throw new CommandParameterException(this, "A value switch cannot be at the end of a command.");

			ParseResult<T> result = parser(args[++index]);
			if (!result.success) throw new CommandParameterException(this, "Failed to parse.");

			Value = result.value;
			HasValue = true;
		}

		/// <inheritdoc />
		public override void Clear()
		{
			Value = default;
			HasValue = false;
		}
	}

	/// <inheritdoc />
	/// <summary>
	/// An option that represents the raw string data of a string input.
	/// </summary>
	public class ValueOption : ValueOption<string>
	{
		/// <inheritdoc />
		public ValueOption(char? shortName, string longName, string description) : base(ParseResult<string>.FromSuccess, shortName, longName, description) { }
	}
}
using System.Collections.Generic;

namespace UMF.Commands.Options
{
	/// <inheritdoc />
	/// <summary>
	/// An option to a command that represents a true/false based on it's prescense.
	/// </summary>
	public class ToggleOption : BaseOption
	{
		/// <inheritdoc />
		public ToggleOption(char? shortName, string longName, string description) : base(shortName, longName, description) { }

		/// <inheritdoc />
		protected override void UpdateSafe(IReadOnlyList<string> args, ref int index)
		{
			HasValue = true;
		}

		/// <inheritdoc />
		public override void Clear()
		{
			HasValue = false;
		}
	}
}
using Newtonsoft.Json.Linq;

namespace UMF.Settings
{
	/// <summary>
	/// An object that accepts JSON settings.
	/// </summary>
	public interface ISettingsAcceptor
	{
		/// <summary>
		/// The fallback if <seealso cref="JsonSettings"/> is null.
		/// </summary>
		JObject DefaultJsonSettings { get; }
		/// <summary>
		/// The current settings.
		/// </summary>
		JObject JsonSettings { get; set; }
	}
}
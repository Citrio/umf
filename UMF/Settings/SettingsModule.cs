using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UMF.Core;
using UMF.Core.Log;
using UMF.Mods;

namespace UMF.Settings
{
	/// <inheritdoc />
	/// <summary>
	/// The module for handling all settings of <see cref="ISettingsAcceptor"/>s.
	/// </summary>
	public sealed class SettingsModule : UmfModule
	{
		/// <summary>
		/// The relative path to the settings directory.
		/// </summary>
		public const string kSettingsDirectory = UmfCore.kUmfDirectory + "settings/";

		/// <summary>
		/// The single instance of <see cref="SettingsModule"/>.
		/// </summary>
		public static SettingsModule Instance { get; private set; }

		private static void Init()
		{
			if (Instance != null) throw new InvalidOperationException("Already instantiated.");

			Instance = new SettingsModule();
		}

		private readonly Dictionary<string, ISettingsAcceptor> settings;

		/// <inheritdoc />
		public override string SettingsId { get; } = "Settings";

		/// <summary>
		/// The currently loaded settings IDs.
		/// </summary>
		public IEnumerable<string> LoadedIDs => settings.Keys.Duplicate();

		private SettingsModule()
		{
			settings = new Dictionary<string, ISettingsAcceptor>();
		}

		private static string GetSettingsPath(string id) =>
			kSettingsDirectory + id + ".json";

		private JObject ReadSettings(string id)
		{
			string path = GetSettingsPath(id);
			if (!File.Exists(path)) return null;

			string content;
			try
			{
				content = File.ReadAllText(path);
			}
			catch (IOException)
			{
				Logger.Error(nameof(SettingsModule), $"Failed to read settings of {id}: could not read the file.");

				return null;
			}

			JObject json;
			try
			{
				json = JObject.Parse(content);
			}
			catch (JsonReaderException e)
			{
				Logger.Error(nameof(SettingsModule), $"Failed to read settings of {id}: error while parsing JSON (line {e.LineNumber}, column {e.LinePosition}).");

				return null;
			}

			return json;
		}

		internal void AddModule(UmfModule module) =>
			AddSettings($"UMF.{module.SettingsId}", module);

		private void AddSettings(string id, ISettingsAcceptor acceptor)
		{
			settings.Add(id, acceptor);

			Logger.UmfDebug(nameof(SettingsModule), $"Added {id} to the settings dictionary.");
		}

		private bool RemoveSettings(string id)
		{
			if (settings.TryGetValue(id, out ISettingsAcceptor acceptor))
			{
				settings.Remove(id);
				acceptor.JsonSettings = null;
				Logger.UmfDebug(nameof(SettingsModule), $"Removed {id} from the settings dictionary.");

				return true;
			}

			return false;
		}

		private void SettingsIDSanityCheck(string id)
		{
			if (id == null) throw new ArgumentNullException(nameof(id));
			if (!settings.ContainsKey(id)) throw new ArgumentException("No settings have the specified ID.", nameof(id));
		}

		/// <summary>
		/// Loads the settings from the file into a <see cref="ISettingsAcceptor"/>.
		/// </summary>
		/// <exception cref="ArgumentNullException"><paramref name="id"/> is null.</exception>
		/// <exception cref="ArgumentException">No settings have the same ID as <paramref name="id"/>.</exception>
		/// <param name="id">The ID of the <see cref="ISettingsAcceptor"/> that was loaded in.</param>
		public void RefreshSettings(string id)
		{
			SettingsIDSanityCheck(id);

			JObject json = ReadSettings(id);
			settings[id].JsonSettings = json;

			Logger.UmfDebug(nameof(SettingsModule), $"Refreshed {id} settings.");
		}

		/// <summary>
		/// Refreshes settings for all loaded <see cref="ISettingsAcceptor"/>.
		/// </summary>
		public void RefreshAllSettings()
		{
			foreach (string id in settings.Keys)
			{
				RefreshSettings(id);
			}
		}

		/// <summary>
		/// Saves the settings from a <see cref="ISettingsAcceptor"/> to the file specified by its ID. False if write error.
		/// </summary>
		/// <exception cref="ArgumentNullException"><paramref name="id"/> is null.</exception>
		/// <exception cref="ArgumentException">No settings have the same ID as <paramref name="id"/>.</exception>
		/// <param name="id">The ID of the <see cref="ISettingsAcceptor"/> that was loaded in.</param>
		public bool SaveSettings(string id)
		{
			string path = GetSettingsPath(id);
			if (!Directory.Exists(Path.GetDirectoryName(path))) return false;

			ISettingsAcceptor acceptor = settings[id];
			try
			{
				File.WriteAllText(path, acceptor.JsonSettings.ToString());
			}
			catch (IOException)
			{
				Logger.Error(nameof(SettingsModule), $"Failed to write settings of {id}.");

				return false;
			}

			return true;
		}

		/// <summary>
		/// Saves settings for all loaded <see cref="ISettingsAcceptor"/>.
		/// </summary>
		public void SaveAllSettings()
		{
			foreach (string id in settings.Keys)
			{
				SaveSettings(id);
			}
		}

		/// <inheritdoc />
		public override bool Load(UmfMod mod)
		{
			if (!base.Load(mod)) return false;

			if (mod is ISettingsAcceptor acceptor)
			{
				AddSettings(mod.InfoAttribute.Id, acceptor);

				if (UmfCore.Initialized) RefreshSettings(mod.InfoAttribute.Id);
			}

			return true;
		}

		/// <inheritdoc />
		public override bool Unload(UmfMod mod)
		{
			if (!base.Unload(mod)) return false;

			RemoveSettings(mod.InfoAttribute.Id);

			return true;
		}

		/// <summary>
		/// Gets the <see cref="ISettingsAcceptor"/> from the specified ID.
		/// </summary>
		/// <exception cref="ArgumentNullException"><paramref name="id"/> is null.</exception>
		/// <param name="id">The ID of the <see cref="ISettingsAcceptor"/> that was loaded in.</param>
		public ISettingsAcceptor this[string id]
		{
			get
			{
				if (id == null) throw new ArgumentNullException(nameof(id));

				return
					settings.TryGetValue(id, out ISettingsAcceptor acceptor)
						? acceptor
						: null;
			}
		}
	}
}
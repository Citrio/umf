using System.Collections.Generic;

namespace UMF.Settings
{
	public class UmfModSettings
	{
		public bool debug;
		public Dictionary<string, string> commands;
		public Dictionary<string, UmfModEventSettings> events;

		public string ApplyCommand(string command)
		{
			if (commands == null || commands.Count == 0) return command;

			return
				commands.TryGetValue(command, out string newCommand)
					? newCommand
					: command;
		}
	}

	public class UmfModEventSettings : UmfPrioritySettings
	{
		public bool enabled = true;
	}
}
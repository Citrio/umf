using System;
using System.Linq;
using UMF.Core.Log;

namespace UMF.Settings
{
	public class UmfPrioritySettings : IComparable<UmfPrioritySettings>
	{
		public string mod;
		public string[] before;
		public string[] after;

		public int CompareTo(UmfPrioritySettings other)
		{
			if (ReferenceEquals(this, other)) return 0;
			if (ReferenceEquals(null, other)) return 1;

			if (before?.Contains(other.mod) ?? false)
			{
				if (other.before?.Contains(mod) ?? false)
				{
					Logger.Error(nameof(UmfModLoadSettings), $"Files {mod} and {other.mod} are both set to load before eachother. Neither will load before eachother.");
					return 0;
				}

				return 1;
			}

			if (after?.Contains(other.mod) ?? false)
			{
				if (other.after?.Contains(mod) ?? false)
				{
					Logger.Error(nameof(UmfModLoadSettings), $"Files {mod} and {other.mod} are both set to load after eachother. Neither will load before eachother.");
					return 0;
				}

				return -1;
			}

			return 0;
		}
	}
}
using System.Collections.Generic;
using System.Linq;
using UMF.Core.Log;

namespace UMF.Settings
{
	public class UmfCoreSettings
	{
		public bool debug = false;
		public UmfModLoadSettings[] modLoadSettings = null;

		public IEnumerable<string> ApplyModLoadSettings(IEnumerable<string> files)
		{
			if (modLoadSettings == null || modLoadSettings.Length == 0) return files;

			Dictionary<string, UmfModLoadSettings> loadSettings =
				modLoadSettings
					.Where(x =>
					{
						if (x.file == null)
						{
							Logger.Error(nameof(UmfCoreSettings), "Mod load settings contains an entry with no file name.");
							return false;
						}

						return true;
					})
					.ToDictionary(x => x.file, x => x);

			return
				files.OrderByDescending(
					x =>
						loadSettings.TryGetValue(x, out UmfModLoadSettings modSettings) ?
							modSettings :
							null
				);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json.Linq;
using UMF.Commands;
using UMF.Core;
using UMF.Core.Log;
using UMF.Events;
using UMF.Settings;

namespace UMF.Mods
{
	/// <summary>
	/// Status of a UMF mod.
	/// </summary>
	public enum ModStatus
	{
		/// <summary>
		/// Mod is not loaded.
		/// </summary>
		Unloaded,
		/// <summary>
		/// Mod was loaded, now unloading.
		/// </summary>
		Unloading,
		/// <summary>
		/// Mod was unloaded, now loading.
		/// </summary>
		Loading,
		/// <summary>
		/// Mod is loaded and ready for use.
		/// </summary>
		Loaded
	}

	/// <inheritdoc />
	/// <summary>
	/// Class that modifies the original behavior of the modded application, on top of the framework. Only one mod may be in a single assembly.
	/// </summary>
	public abstract class UmfMod : ISettingsAcceptor
	{
		/// <summary>
		/// The fallback of <seealso cref="ModSettings"/> if it is null.
		/// </summary>
		public static UmfModSettings DefaultModSettings { get; } = new UmfModSettings();


		private readonly UmfCommand[] commandsToLoad;

		/// <summary>
		/// The info attribute placed on this mod.
		/// </summary>
		public UmfModInfoAttribute InfoAttribute { get; }
		/// <summary>
		/// The current load status of the mod.
		/// </summary>
		public ModStatus Status { get; internal set; }

		/// <inheritdoc />
		public virtual JObject DefaultJsonSettings { get; } = JObject.FromObject(DefaultModSettings);

		private JObject jsonSettings;
		/// <inheritdoc />
		public JObject JsonSettings
		{
			get => jsonSettings ?? DefaultJsonSettings;
			set
			{
				try
				{
					OnSettingsSet(value);
				}
				catch (Exception e)
				{
					Logger.Error(nameof(UmfMod), $"Failed to set settings of {this}:\n{e}");
				}

				ModSettings = value?.ToObject<UmfModSettings>();
				jsonSettings = value;
			}
		}

		private UmfModSettings modSettings;
		/// <summary>
		/// The settings that all UMF mods must have.
		/// </summary>
		public UmfModSettings ModSettings
		{
			get => modSettings ?? DefaultModSettings;
			private set => modSettings = value;
		}

		/// <summary>
		/// The commands declared by this mod.
		/// </summary>
		public IEnumerable<UmfCommand> Commands => commandsToLoad.Duplicate();

		/// <summary>
		/// Creates a new instance of <see cref="UmfMod"/>.
		/// </summary>
		/// <param name="commands">The commands to declare for this mod.</param>
		protected UmfMod(params UmfCommand[] commands)
		{
			commandsToLoad = commands ?? new UmfCommand[0];

			InfoAttribute = GetType().GetCustomAttribute<UmfModInfoAttribute>() ?? new UmfModInfoAttribute();
		}

		/// <summary>
		/// Fired when <seealso cref="JsonSettings"/> is set.
		/// </summary>
		/// <param name="json">The value of <seealso cref="JsonSettings"/> before it is set.</param>
		protected virtual void OnSettingsSet(JObject json) { }

		/// <summary>
		/// Shortcut to <see cref="EventModule"/>'s generic handle method.
		/// </summary>
		protected TDelegate HandleEvent<TDelegate>() where TDelegate : Delegate =>
			EventModule.Instance.HandleCalledEvent<TDelegate>(Assembly.GetCallingAssembly());

		/// <summary>
		/// Shortcut to <see cref="EventModule"/>'s parameterized handle method.
		/// </summary>
		protected Delegate HandleEvent(Type delegateType) =>
			EventModule.Instance.HandleCalledEvent(delegateType, Assembly.GetCallingAssembly());

		/// <summary>
		/// Saves the current <seealso cref="JsonSettings"/> to the settings file. False if write failed.
		/// </summary>
		protected bool SaveSettings() =>
			SettingsModule.Instance.SaveSettings(InfoAttribute.Id);

		/// <summary>
		/// Loads the settings file to <seealso cref="JsonSettings"/>.
		/// </summary>
		protected void RefreshSettings() =>
			SettingsModule.Instance.RefreshSettings(InfoAttribute.Id);

		/// <summary>
		/// Shortcut to <see cref="Logger"/>'s info method.
		/// </summary>
		protected void Info(string message) =>
			Logger.Info(InfoAttribute.Id, message);

		/// <summary>
		/// Shortcut to <see cref="Logger"/>'s warn method.
		/// </summary>
		protected void Warn(string message) =>
			Logger.Warning(InfoAttribute.Id, message);

		/// <summary>
		/// Shortcut to <see cref="Logger"/>'s error method.
		/// </summary>
		protected void Error(string message) =>
			Logger.Error(InfoAttribute.Id, message);

		/// <summary>
		/// Shortcut to <see cref="Logger"/>'s debug method, only called if the <seealso cref="ModSettings"/> has debug set to <code>true</code>.
		/// </summary>
		protected void Debug(string message)
		{
			if (ModSettings.debug)
			{
				Logger.Debug(InfoAttribute.Id, message);
			}
		}

		/// <summary>
		/// Formats the ID and name into a single string.
		/// </summary>
		public override string ToString() =>
			InfoAttribute.Name == null ?
				InfoAttribute.Id :
				$"{InfoAttribute.Name} ({InfoAttribute.Id})";
	}

	/// <inheritdoc />
	/// <summary>
	/// Class that modifies the original behavior of the modded application, on top of the framework with typed settings.
	/// </summary>
	/// <typeparam name="TSettings">Type of the settings.</typeparam>
	public abstract class UmfMod<TSettings> : UmfMod where TSettings : UmfModSettings, new()
	{
		/// <summary>
		/// The default typed settings.
		/// </summary>
		public static TSettings DefaultSettings { get; } = new TSettings();


		/// <inheritdoc />
		public override JObject DefaultJsonSettings { get; } = JObject.FromObject(DefaultSettings);

		private TSettings settings;
		/// <summary>
		/// The typed settings.
		/// </summary>
		public TSettings Settings
		{
			get => settings ?? DefaultSettings;
			protected set =>
				JsonSettings =
					value == null
						? null
						: JObject.FromObject(value);
		}

		/// <inheritdoc />
		protected override void OnSettingsSet(JObject json) =>
			settings = json?.ToObject<TSettings>();
	}
}
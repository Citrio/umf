using System;

namespace UMF.Mods
{
	/// <inheritdoc />
	/// <summary>
	/// Provides additional info on a UMF mod.
	/// </summary>
	public class UmfModInfoAttribute : Attribute
	{
		/// <summary>
		/// The unique ID of the mod, determined by the assembly and file name.
		/// </summary>
		public string Id { get; internal set; }
		/// <summary>
		/// The version of the mod, determined by the assembly version.
		/// </summary>
		public Version AssemblyVersion { get; internal set; }
		/// <summary>
		/// The human-readable name of the mod.
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// The description of what the mod does.
		/// </summary>
		public string Description { get; set; }
		/// <summary>
		/// The creator(s) of the mod.
		/// </summary>
		public string Author { get; set; }
	}
}
using System;
using System.IO;
using System.Reflection;
using dnlib.DotNet;
using UMF.Patcher;
using Debug = UnityEngine.Debug;

namespace UMF.Extension.RoR2.Hooks
{
	public class RoR2Patcher : BasePatcher
	{
		public override string CurrentVersion { get; }
		public override string DefaultPath { get; }
		public override MethodDef InitMethod { get; protected set; }
		public override MethodDef ModLoader { get; }
		public override int ILIndex { get; }

		public RoR2Patcher()
		{
			CurrentVersion = "0.0.0";
			DefaultPath = "../../Risk of Rain 2_Data/Managed/Assembly-CSharp.dll";

			ModLoader = ModuleDefMD.Load(typeof(RoR2Payload).Module).Find(typeof(RoR2Payload).FullName, true).FindMethod(nameof(RoR2Payload.UmfModLoader));
			ILIndex = 125;
		}

		public override void LoadModule(ModuleDefMD module)
		{
			InitMethod = module.Find("RoR2.Console", true).FindMethod("Awake");
		}

		public override void Patch(string path)
		{
			const string mscorlib = "mscorlib.dll";
			File.Copy(mscorlib, Path.Combine(Path.GetDirectoryName(path), mscorlib), true);

			base.Patch(path);
		}

		private static class RoR2Payload
		{
			internal static void UmfModLoader()
			{
				Debug.Log("Running UMF Mod Loader...");

				try
				{
					foreach (string file in Directory.GetFiles("UMF/bin/", "*.dll"))
					{
						Debug.Log($"Loading {file}...");

						Assembly.LoadFrom(file);
					}

					foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
					{
						Type core = assembly.GetType("UMF.Core.UMFCore");
						if (core != null)
						{
							MethodInfo method = core.GetMethod("Init", BindingFlags.NonPublic | BindingFlags.Static);
							if (method == null) throw new MissingMethodException("The Init method of UmfCore does not exist.");

							method.Invoke(null, null);
						}
					}
				}
				catch (Exception e)
				{
					Debug.LogError($"Failed to load UMF:\n{e}");
				}
			}
		}
	}
}
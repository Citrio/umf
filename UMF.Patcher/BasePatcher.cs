using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using dnlib.DotNet;
using OpCodes = dnlib.DotNet.Emit.OpCodes;

namespace UMF.Patcher
{
	public abstract class BasePatcher
	{
		protected static ModuleDefMD PatcherModule { get; }

		protected static TypeDef IgnoredAttribute { get; }
		protected static TypeDef PatchedAttribute { get; }
		protected static TypeDef InjectedAttribute { get; }

		protected static MethodDef PatchedAttributeCtor { get; }
		protected static MethodDef InjectedAttributeCtor { get; }

		static BasePatcher()
		{
			PatcherModule = ModuleDefMD.Load(typeof(BasePatcher).Module);
			IgnoredAttribute = PatcherModule.Find(typeof(InjectorIgnored).FullName, true);
			PatchedAttribute = PatcherModule.Find(typeof(UMFPatched).FullName, true);
			InjectedAttribute = PatcherModule.Find(typeof(UMFInjected).FullName, true);

			const string ctor = ".ctor";
			PatchedAttributeCtor = PatchedAttribute.FindMethod(ctor);
			InjectedAttributeCtor = InjectedAttribute.FindMethod(ctor);
		}

		public abstract string CurrentVersion { get; }
		public abstract string DefaultPath { get; }

		private ModuleDefMD module;
		public ModuleDefMD Module
		{
			get => module;
			set
			{
				LoadModule(value);
				module = value;
			}
		}
		public abstract MethodDef InitMethod { get; protected set; }
		public abstract MethodDef ModLoader { get; }
		public abstract int ILIndex { get; }

		public abstract void LoadModule(ModuleDefMD module);

		public static BasePatcher Create(Type type) =>
			(BasePatcher) Activator.CreateInstance(type);

		public static IEnumerable<Type> AllPatcherTypes()
		{
			foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				foreach (Type type in assembly.GetTypes())
				{
					if (!type.IsAbstract && typeof(BasePatcher).IsAssignableFrom(type)) yield return type;
				}
			}
		}

		protected static void ForSuccessLoop<T>(IList<T> values, Func<T, bool> predicate)
		{
			for (int i = 0; i < values.Count;)
			{
				if (!predicate(values[i])) i++;
			}
		}

		protected void EjectAllMembers(TypeDef target, Func<IMemberDef, bool> predicate)
		{
			ForSuccessLoop(target.Fields, field =>
			{
				if (predicate(field))
				{
					target.Fields.Remove(field);
					return true;
				}

				return false;
			});
			ForSuccessLoop(target.Properties, property =>
			{
				if (predicate(property))
				{
					target.Properties.Remove(property);
					return true;
				}

				return false;
			});
			ForSuccessLoop(target.Methods, method =>
			{
				if (predicate(method))
				{
					target.Methods.Remove(method);
					return true;
				}

				return false;
			});
			ForSuccessLoop(target.NestedTypes, type =>
			{
				if (predicate(type))
				{
					target.NestedTypes.Remove(type);
					return true;
				}

				return false;
			});
		}

		protected void EjectAllMembers(TypeDef target) =>
			EjectAllMembers(target, member => member.CustomAttributes.Any(x => x.TypeFullName == InjectedAttribute.FullName));

		protected void Eject(TypeDef target) =>
			target.Module.Types.Remove(target);

		protected void EjectAllTypes(Func<TypeDef, bool> predicate)
		{
			for (int i = 0; i < Module.Types.Count;)
			{
				TypeDef type = Module.Types[i];

				if (predicate(type))
				{
					Eject(type);
				}
				else
				{
					i++;
				}
			}
		}

		protected void EjectAllTypes() =>
			EjectAllTypes(type => type.CustomAttributes.Any(x => x.TypeFullName == InjectedAttribute.FullName));

		protected void InjectAllMembers(TypeDef source, TypeDef target, Func<IMemberDef, bool> predicate)
		{
			bool NewPredicate(IMemberDef member)
			{
				if (predicate(member))
				{
					member.CustomAttributes.Add(new CustomAttribute(InjectedAttributeCtor));

					return true;
				}

				return false;
			}

			ForSuccessLoop(source.Fields, field =>
			{
				if (NewPredicate(field))
				{
					field.DeclaringType = target;
					return true;
				}

				return false;
			});
			ForSuccessLoop(source.Properties, property =>
			{
				if (NewPredicate(property))
				{
					property.DeclaringType = target;
					return true;
				}

				return false;
			});
			ForSuccessLoop(source.Methods, method =>
			{
				if (method.Name == ".ctor") return false;

				if (NewPredicate(method))
				{
					method.DeclaringType = target;
					return true;
				}

				return false;
			});
			ForSuccessLoop(source.NestedTypes, type =>
			{
				if (NewPredicate(type))
				{
					type.DeclaringType = target;
					return true;
				}

				return false;
			});
		}

		protected void InjectAllMembers(TypeDef source, TypeDef target) =>
			InjectAllMembers(source, target, member => member.CustomAttributes.All(x => x.AttributeType != IgnoredAttribute));

		protected void Inject(TypeDef type)
		{
			EjectAllMembers(type, member => member.CustomAttributes.Any(x => x.AttributeType == IgnoredAttribute));

			type.Module.Types.Remove(type);
			module.Types.Add(type);

			type.CustomAttributes.Add(
				new CustomAttribute(
					InjectedAttributeCtor
				)
			);
		}

		public virtual void Patch(string path)
		{
			Inject(InjectedAttribute);
			Inject(PatchedAttribute);

			int index = ILIndex;

			InjectAllMembers(ModLoader.DeclaringType, InitMethod.DeclaringType);
			InitMethod.Body.Instructions.Insert(index++, OpCodes.Call.ToInstruction(ModLoader));

			InitMethod.CustomAttributes.Add(
				new CustomAttribute(
					PatchedAttributeCtor,
					new[]
					{
						new CAArgument(InitMethod.Module.CorLibTypes.String, CurrentVersion),
						new CAArgument(InitMethod.Module.CorLibTypes.Int32, ILIndex),
						new CAArgument(InitMethod.Module.CorLibTypes.Int32, index)
					}
				)
			);
		}

		public virtual void Unpatch(string path)
		{
			UMFPatched patchInfo = Info();
			if (patchInfo == null) return;

			EjectAllTypes();
			EjectAllMembers(InitMethod.DeclaringType);

			int length = patchInfo.EndIndex - patchInfo.StartIndex;
			for (int i = 0; i < length; i++) InitMethod.Body.Instructions.RemoveAt(patchInfo.StartIndex);
			InitMethod.CustomAttributes.Remove(InitMethod.CustomAttributes.FirstOrDefault(x => x.TypeFullName == PatchedAttribute.FullName));
		}

		public virtual UMFPatched Info() =>
			UMFPatched.Create(InitMethod.CustomAttributes.FirstOrDefault(x => x.TypeFullName == PatchedAttribute.FullName));
	}
}
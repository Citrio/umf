using System;
using dnlib.DotNet;

namespace UMF.Patcher
{
	public sealed class InjectorIgnored : Attribute { }

	[AttributeUsage(AttributeTargets.Method)]
	public sealed class UMFPatched : Attribute
	{
		public string Version { get; }
		public int StartIndex { get; }
		public int EndIndex { get; }

		public UMFPatched(string version, int startIndex, int endIndex)
		{
			Version = version;
			StartIndex = startIndex;
			EndIndex = endIndex;
		}

		[InjectorIgnored]
		public static UMFPatched Create(CustomAttribute attribute) =>
			attribute == null
				? null
				: new UMFPatched(
					(UTF8String) attribute.ConstructorArguments[0].Value,
					(int) attribute.ConstructorArguments[1].Value,
					(int) attribute.ConstructorArguments[2].Value
				);
	}

	public sealed class UMFInjected : Attribute { }
}
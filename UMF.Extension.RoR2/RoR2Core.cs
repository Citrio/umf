using System;
using RoR2;
using UMF.Core;
using UMF.Core.Log;
using UnityEngine;
using Logger = UMF.Core.Log.Logger;

namespace UMF.Extension.RoR2
{
	[UmfExtension]
	public class RoR2Core : UmfModule
	{
		public static RoR2Core Instance { get; private set; }

		public override string SettingsId { get; } = "Extension.RoR2";

		private static void Init()
		{
			if (Instance != null) throw new InvalidOperationException("Already instantiated.");

			Instance = new RoR2Core();

			RoR2Application.isModded = true;
			Logger.Invoked += message =>
			{
				string noSeverity = message.ToString(LogMessage.Format.Source | LogMessage.Format.Message);
				switch (message.severity)
				{
					case Logger.Severity.Error:
						Debug.LogError(noSeverity);
						break;

					case Logger.Severity.Warning:
						Debug.LogWarning(noSeverity);
						break;

					default:
						Debug.Log(noSeverity);
						break;
				}
			};
		}
	}
}
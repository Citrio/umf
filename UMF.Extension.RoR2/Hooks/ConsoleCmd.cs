﻿using RoR2;
using UMF.Commands;
using UMF.Commands.Results;
using UMF.Events;
using UnityEngine;
using Console = RoR2.Console;

namespace UMF.Extension.RoR2.Hooks
{
    public class ConsoleCmd : UmfHarmonyEvent
    {
        public delegate void OnConsoleCmd(string cmd, NetworkUser user, ref bool handled);

        public ConsoleCmd() : base(typeof(OnConsoleCmd), typeof(Console), nameof(Console.SubmitCmd)) { }

        private static bool Prefix(NetworkUser user, string cmd)
        {
            if (Locked) return true;

            bool handled = false;
            Handle<OnConsoleCmd>()?.Invoke(cmd, user, ref handled);
            if (handled) return false;

            if (user.isLocalPlayer)
            {
                CommandResult result = CommandModule.Instance.Handle(cmd);

                if (result.found)
                {
                    if (result.success)
                    {
                        Debug.Log(result.message);
                    }
                    else
                    {
                        Debug.LogError(result.message);
                    }

                    return false;
                }
            }

            return true;
        }
    }
}

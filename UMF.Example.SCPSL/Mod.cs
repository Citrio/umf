﻿using UMF.Mods;

namespace UMF.Example.SCPSL
{
	[UmfModInfo(
		Name = "SCP Secret Laboratory Example",
		Author = "Androx",
		Description = "An example of UMF using SCPSL"
	)]
	public class Mod : UmfMod
	{
		public Mod() : base(new EchoCommand("echo")) { }

		public void OnConsoleCmd(string cmd)
		{
			Info($"Used command: {cmd}");
		}
	}
}